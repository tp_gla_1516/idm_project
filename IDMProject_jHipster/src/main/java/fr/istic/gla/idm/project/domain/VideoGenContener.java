package fr.istic.gla.idm.project.domain;

import fr.istic.gla.idm.transformation.VideoGenTransformation;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created by lautre on 13/01/16.
 */
@Component
@Scope("session")
public class VideoGenContener {

    private VideoGenTransformation videoGenTransformation;

    public VideoGenContener(String chemin) {
        this.videoGenTransformation = new VideoGenTransformation(chemin);
    }

    public VideoGenTransformation getVideoGenTransformation() {
        return videoGenTransformation;
    }
}
