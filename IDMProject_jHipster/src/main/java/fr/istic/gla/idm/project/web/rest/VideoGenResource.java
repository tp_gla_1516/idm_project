package fr.istic.gla.idm.project.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import fr.istic.gla.idm.project.domain.VideoGenContener;
import fr.istic.gla.idm.transformation.PlaylistTransformation;
import fr.istic.gla.idm.transformation.VideoGenTransformation;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.ArrayList;

@RestController
@RequestMapping("/videoGen")
@SessionAttributes(value="videoGenTransformation", types={VideoGenTransformation.class})
public class VideoGenResource {

    private final Logger log = LoggerFactory.getLogger(VideoGenResource.class);

    @Inject
    private VideoGenContener videoGenContener;
    private String namePlaylistVideoGen;

    private final String dirWeb = "src/main/webapp";

    @RequestMapping(value = "/generate/configPlaylistPage/{namePlaylistVideoGen}", method = RequestMethod.GET)
    public void generateConfigPlaylistPage(@PathVariable String namePlaylistVideoGen) {
        this.namePlaylistVideoGen = namePlaylistVideoGen;
        log.info("Génère la page html pour la selection de la playlist pour : "+ this.namePlaylistVideoGen);

        String chemin = dirWeb+"/resources/playlistVideoGen/"+this.namePlaylistVideoGen+".videoGen";
        if ( new File(chemin).exists() ) {
            videoGenContener = new VideoGenContener(chemin);
            VideoGenTransformation videoGenTransformation = videoGenContener.getVideoGenTransformation();
            videoGenTransformation.generator(dirWeb+"/views/vignette.html", VideoGenTransformation.GeneratorType.HTMLVignette);
        }
        else{
            log.info("j'existe pas :'(");
            //TODO Générer une page d'erreur ;)
            //vgTrans.generator(dirWeb+"/views/vignette.html", VideoGenTransformation.GeneratorType.HTMLVignette);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/generate/playerPage", method = RequestMethod.POST, consumes="application/json")
    public void generatePlayerPage(@RequestBody String string) throws JSONException {
        log.info("Génère la page html du player");
        VideoGenTransformation videoGenTransformation = videoGenContener.getVideoGenTransformation();

        VideoGenTransformation.TransformationTarget type;
        ArrayList<String> listId = creationArrayID((JSONObject) JSONValue.parse(string));
        log.debug(listId.toString());
        if(listId.isEmpty())
            type = VideoGenTransformation.TransformationTarget.PlaylistRandom;
        else
            type = VideoGenTransformation.TransformationTarget.PlaylistNotRandom;
        String dirPlaylist = "/resources/playlist";
        PlaylistTransformation playlistTransformation = new PlaylistTransformation(
            videoGenTransformation.transformator(type, listId),
            this.namePlaylistVideoGen,System.getProperty("user.dir")+"/"+dirWeb+dirPlaylist,
            dirPlaylist
        );
        playlistTransformation.generator(dirWeb+"/views/player.html", PlaylistTransformation.GeneratorType.HTMLPlayerPage);
    }

    @RequestMapping(value = "/listPlaylists", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public String getListPlaylist() throws JsonProcessingException, JSONException {
        log.info("Recuparation de la liste des playlists sur le serveur");

        File folder = new File(dirWeb+"/resources/playlistVideoGen/");
        File[] listOfFiles = folder.listFiles();
        JSONArray json = new JSONArray();
        for (File file : listOfFiles != null ? listOfFiles : new File[0]) {
            if (file.isFile() && FilenameUtils.getExtension(file.getAbsolutePath()).equals("videoGen")) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", file.getName().replace(".videoGen",""));
                json.add(jsonObject);
            }
        }
        return json.toJSONString();
    }

    @RequestMapping(value = "/listVideos", method = RequestMethod.GET, produces="application/json")
    @ResponseBody
    public String getListVideos() throws JSONException {
        log.info("Recuparation de la liste des videos sur le serveur");

        File folder = new File(dirWeb+"/resources/videos/");
        File[] listOfFiles = folder.listFiles();
        JSONArray json = new JSONArray();
        for (File file : listOfFiles != null ? listOfFiles : new File[0]) {
            if (file.isFile() && FilenameUtils.getExtension(file.getAbsolutePath()).equals("mp4")) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", file.getName().replace(".mp4","").replace("_"," "));
                jsonObject.put("urlImg", "resources/videos/Vignettes/"+file.getName().replace(".mp4",".png"));
                jsonObject.put("url", file.getAbsolutePath());
                json.add(jsonObject);
            }
        }
        return json.toJSONString();
    }

    @CrossOrigin
    @RequestMapping(value = "/create/playlist", method = RequestMethod.POST, consumes="application/json")
    public void createPlaylist(@RequestBody String string) throws JSONException {
        log.info("création d'un model videoGen");
        VideoGenTransformation.createNewVideoGen(dirWeb+"/resources/playlistVideoGen/",string);
    }

    private ArrayList<String> creationArrayID(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = (JSONArray) jsonObject.get("listId");
        ArrayList<String> listId = new ArrayList<>();
        for (Object aJsonArray : jsonArray) {
            JSONObject jsonObject1 = (JSONObject) aJsonArray;
            listId.add((String) jsonObject1.get("id"));
        }
        return listId;
    }

}
