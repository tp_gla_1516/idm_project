/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package fr.istic.gla.idm.project.web.rest.dto;
