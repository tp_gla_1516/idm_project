'use strict';

/**
 * @ngdoc overview
 * @name siteWebApp
 * @description
 * # siteWebApp
 *
 * Main module of the application.
 */
angular
  .module('siteWebApp', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch',
      'ui.bootstrap',
      'nya.bootstrap.select'
  ])
  .config(function ($routeProvider) {
    $routeProvider
        .when('/player', {
            templateUrl: 'views/player.html',
            controller: 'playerCtrl',
            controllerAs: 'player'
        }).when('/creationPlaylist', {
            templateUrl: 'views/creationPlaylist.html',
            controller: 'createModelCtrl',
            controllerAs: 'createModel'
        }).when('/', {
            templateUrl: 'views/main.html',
            controller: 'mainCtrl',
            controllerAs: 'main'
        }).when('/choosePlaylist', {
            templateUrl: 'views/vignette.html',
            controller: 'chooseVideoCtrl',
            controllerAs: 'chooseVideoCtrl'
          })
          .otherwise({
            redirectTo: '/'
          });
  });
