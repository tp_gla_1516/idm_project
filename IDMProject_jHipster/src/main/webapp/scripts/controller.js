/**
 * Created by lautre on 06/12/15.
 */

angular.module('siteWebApp')
  .controller('mainCtrl', function ($scope,$http,$location) {
      $scope.playlists = [];
      $http.get("/videoGen/listPlaylists").then(function (data) {
          $scope.playlists = data.data;
      });
      $scope.selectedPlaylist = "";
      $scope.genListVignette = function (){
          if ($scope.selectedPlaylist.name != undefined) {
              $http.get("/videoGen/generate/configPlaylistPage/" + $scope.selectedPlaylist.name).then(function () {
                  $location.path("/choosePlaylist")
              });
          }
      }
  })
    .controller('createModelCtrl', function ($scope,$http, $uibModal,$location) {
        $scope.model={name : "New Model" , sequence : []};


        $http.get("/videoGen/listVideos").then(function (data) {
            $scope.videos = data.data;
        });

        $scope.create = function (){
            $http.post('/videoGen/create/playlist', $scope.model ).then(function () {
                $location.path("/")
            });
        };

        $scope.clearSequences = function (){
            $scope.model.sequence.splice(0,  $scope.model.sequence.length);
        };

        $scope.removeSequence = function(index){
            $scope.model.sequence.splice(index, 1);
        };

        $scope.openModalSequence = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/partials/createSequenceModal.html',
                controller: 'createSequenceCtrl',
                size: 'lg',
                resolve: {
                    videos: function () {
                        return $scope.videos;
                    }
                }
            });

            modalInstance.result.then(function (sequence) {
                $scope.model.sequence.push(sequence);
            }, function () { });
        };
    })
    .controller('createSequenceCtrl', function ($scope,$uibModalInstance,$filter,videos) {
        $scope.videos = $filter('orderBy')(videos, 'name', false);
        $scope.sequence = {};
        $scope.selectedVideos = [];
        $scope.selectedVideo = {};
        $scope.typeSequence = {};


        $scope.createNewSequence = function(){
            if ($scope.typeSequence === "mandatory" || $scope.typeSequence === "optional" ){
                $scope.sequence = {type : $scope.typeSequence, sequence : $scope.selectedVideo};
                return true;
            }
            else if ($scope.typeSequence === "alternative" ) {
                $scope.sequence = {type : $scope.typeSequence, listSequence : $scope.selectedVideos };
                return true;
            }
            else return false
        };

        $scope.valider = function () {
           if($scope.createNewSequence())
                $uibModalInstance.close($scope.sequence);
            else $scope.cancel();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('chooseVideoCtrl', function ($scope,$http,$location) {
    $scope.getNSendListIdVideo = function (){
      $scope.alerts = [];
      var listJson = [];
      var nbSeqAltManquante = 0;
      var listSeqAltManquante = [];

       angular.element(".seqId").each(function( index ) {
         var inputH = $(this);
         if (inputH.hasClass("mandatory")){
             listJson.push({"id" : inputH.val()});
         } else if (inputH.hasClass("optional") && inputH.hasClass("Oui")){
             listJson.push({"id" : inputH.val()});
         } else if (inputH.hasClass("alternative")){
           var value = inputH.val();
             if (value != "") listJson.push({"id" : value});
             else {
             nbSeqAltManquante++;
             listSeqAltManquante.push(index);
           }
         }
       });

      if (nbSeqAltManquante == 1) {
        $scope.alerts.push({
          'type' : 'warning',
          'msg' : 'Selectioner une alternative pour la séquence '+listSeqAltManquante});
      }
      else if (nbSeqAltManquante > 1) {
        $scope.alerts.push({
          'type' : "warning",
          'msg' : "Selectioner une alternative pour les séquences "+listSeqAltManquante});
      }
      else {
          $http.post('/videoGen/generate/playerPage', {"listId" : listJson}).then(function () {
              $location.path('/player');
          });
      }
    }
    $scope.playlistRandom = function (){
        $http.post('/videoGen/generate/playerPage', {"listId" : []}).then(function () {
            $location.path('/player');
        });
    }
  })
    .controller('playerCtrl', function ($scope) {
    var playlist = angular.element("#playlist").val();
    $scope.optionPlayer = {
      wmode: 'direct',
      plugins: {
        httpstreaming: {
          url: '../flashLibs/flashlsFlowPlayer.swf',
          hls_debug : false,
          hls_debug2 : false,
          hls_lowbufferlength : 3,
          hls_minbufferlength : -1,
          hls_maxbufferlength : 300,
          hls_startfromlevel : -1,
          hls_seekfromlevel : -1,
          hls_live_flushurlcache : false,
          hls_seekmode : "ACCURATE",
          hls_fragmentloadmaxretry : -1,
          hls_manifestloadmaxretry : -1,
          hls_capleveltostage : false,
          hls_maxlevelcappingmode : "downscale"
        }
      },
      clip: {
        accelerated: true,
        url: playlist,
        urlResolvers: "httpstreaming",
        lang: "fr",
        provider: "httpstreaming",
        autoPlay: false,
        autoBuffering: true
      },
      log: {
        level: 'debug',
        filter: 'org.flowplayer.controller.*'
      }
    };
    flowplayer("player", "../flashLibs/flowplayer.swf", $scope.optionPlayer);
  });
