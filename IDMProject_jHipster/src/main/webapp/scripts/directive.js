/**
 * Created by lautre on 06/12/15.
 */

angular.module('siteWebApp')
  .directive('displayMandatory', function() {
    return {
      restrict: 'E',
      scope: {
        idVideo: '@id',
        index: '@index',
        nom: '@nom',
        url: '@url'
      },
      templateUrl: 'views/templates/mandatory-template.html'
    };
  })
  .directive('displayOptional', function() {
    return {
      restrict: 'E',
      scope: {
        idVideo: '@id',
        index: '@index',
        nom: '@nom',
        url: '@url'
      },
      templateUrl: 'views/templates/optional-template.html'
    };
  })
  .directive('displayAlternative', function() {
    return {
      restrict: 'E',
      transclude: "video",
      scope: {
        idVideo: '@id',
        index: '@index'
      },
      controller: function($scope) {
        $scope.videos=[];
        this.addVideoAlt = function (nom,url,id){
          $scope.videos.push({'nom': nom,'url': url,'id': id,'selected':false});
        };
        $scope.setVideoSelect = function(video){
          $scope.videoSelect = video
        }
      },
      templateUrl: 'views/templates/alternative-template.html'
    };
  })
  .directive('displayAlternativeVideo', function() {
    return {
      restrict: 'E',
      require: "^displayAlternative",
      scope: {
        idVideo: '@id',
        nom: '@nom',
        url: '@url',
        index: '@index'
      },
      link :function($scope,$element,$attrs,controllerParent){
        controllerParent.addVideoAlt($attrs.nom,$attrs.url,$attrs.id);
      }
    };
  })
    ;
