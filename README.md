Projet IDM 2015-2016
===================
Projet de Génération de **vidéos aléatoire** en utilisant **Xtext**, **Xtend** & **JHipster**.

----------
[TOC]

----------

##1. Présentation

Ce projet est un site web qui permet de générer des vidéos aléatoire à partir d'un fichier décrivant un model videoGen. Ce model à été définit via le langage XText et des transformations pour ce model on été développées en XTend. Tout la stack technique du site web à été générer via avec jHipster.

----------

##2. Fonctionnalités 

Le site offre plusieurs fonctionnalités qui sont les suivantes :
- Créer un model videogen à partir des vidéos présente sur le serveur.
- Générer une playlist aléatoire basée sur un model sélectionné.
- Générer une playlist configurer à partir des choix de l'utilisateur.
- Jouer une vidéo à partir d'une playlist générée

----------

##3. Build

###3.1. Général 
Pour utiliser le projet il suffit d’exécuter le fichier disponible [ici](https://bitbucket.org/snippets/tp_gla_1516/GdX5b), Il va cloner le dépôt et installer les dépendances maven, npm et bower. 
Les vidéos ne sont pas fournie il faut les rajouter dans le dossier `IDMProject_jHipster/src/main/webapp/resources/videos`.
Il faudra ensuite aller à la racine du projet *IDMProject_jHipster* et executer la commande `mvn spring-boot:run`, pour lancer le projet.
Vous pouvez utiliser l'application. Commencer par créer un model puis commencer à générer.

###3.2. Modification
####3.2.1. Model *videoGen* 

Si le model videoGen  est modifié il faudra le packager en un jar exécutable nommé *''videogen.jar* et le placer dans le dossier */lib*. puis exécuter cette commande à la racine du projet `mvn install:install-file -Dfile=lib/videogen.jar -DgroupId=fr.istic.gla.idm -DartifactId=videogen -Dversion=1.0.0 -Dpackaging=jar`

####3.2.2. Model *playlist*  
Si le model playlist  est modifié il faudra le packager en un jar exécutable nommé *''playlist.jar* et le placer dans le dossier */lib*. puis exécuter cette commande à la racine du projet `mvn install:install-file -Dfile=lib/playlist.jar -DgroupId=fr.istic.gla.idm -DartifactId=playlist -Dversion=1.0.0 -Dpackaging=jar` 
 
####3.2.3. Module *transformation*  
Si le module transformation est modifié il faut exécuter `mvn install` à la racine du module.

####3.2.4. Module *jhipster* 
Si le module transformation est modifié il faut exécuter `mvn spring-boot:run` à la racine du module pour le relancer.

----------

##4. Bug Connus

Il y a certain bug qui n'ont pas encore été résolu :
- Un problème avec le cache, Quand on revient sur la page de configuration après un changement de model, il faut recharger la page pour avoir la bonne page.
- Régler les derniers soucis de chemin absolut en autre dans les fichiers  du model VideoGen.

----------

##5. Améliorations possible

J'ai pensé à plusieurs améliorations mais je n'avais pas le temps de les mettre en place. En voici quelques'une :
- Rendre l'application multi-Utilisateur (Pas forcement logger).
- Ajouter un affichage des différentes vidéos contenu dans la vidéos générer a gauche du lecteur.
- Ajouter le code front-end pour la création de compte admin et utilisateur.
- Utiliser une base de donnée pour améliorer le stockage des vidéos et models.
- Possibilité de partager une vidéo via un lien.
- Possibilité de télécharger une vidéo.
- Industrialiser le build (Maven).
- Crée un docker avec l'application.
- Ajouter une message pour prévenir que les videos sont en conversion 
- etc ...