package fr.istic.gla.idm.transformation

import java.io.IOException
import java.io.InputStreamReader
import java.io.BufferedReader

class UtilsFfmpeg {
	def static boolean convertToTs(String urlIn){
		val urlOut = UtilsTransformation.computeUrlOut(urlIn,TypeUrl.ConvertVideo,TypeTarget.FFMpeg)
		if(!UtilsTransformation.alreadyExist(urlOut)){
			val rt = Runtime::runtime
			val cmd = #[
				"/bin/sh", "-c", '''ffmpeg -i «urlIn» -strict -2 -vcodec libx264 -acodec aac -f mpegts «urlOut»'''
				]
			try {
				var Process p = rt.exec(cmd)
		       	p.waitFor();
				return true
			} catch (IOException e) {
				e.printStackTrace
				return false
			}
		}
		return true
	}

	def static createVignette(String urlIn){
		val urlOut = UtilsTransformation.computeUrlOut(urlIn,TypeUrl.Vignette,TypeTarget.FFMpeg)
		if(!UtilsTransformation.alreadyExist(urlOut)){
			val rt = Runtime::runtime
			val cmd = #[
				"/bin/sh", "-c", '''ffmpeg -y -i  «urlIn»  -r 1 -t 00:00:01 -ss 00:00:01 -f image2  «urlOut» '''
				]
			try {
				var Process p = rt.exec(cmd)
		       	p.waitFor();
			} catch (IOException e) {
				e.printStackTrace
			}
		}
		return urlOut
	}
	def static getDuration(String filename){
		val rt = Runtime::runtime
		val cmd = #[ "/bin/sh", "-c", '''ffprobe -i «filename» -show_format | grep duration''']

		try {
			val p = rt.exec(cmd)
			val stdInput = new BufferedReader(
				new InputStreamReader(p.inputStream))
			val durationLine = stdInput.readLine
			return durationLine.split("=").get(1)

		} catch (IOException e) {
			e.printStackTrace
		}
	}
}
