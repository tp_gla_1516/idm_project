package fr.istic.gla.idm.transformation

enum TypeUrl {
	ConvertVideo,
	Vignette
}

enum TypeTarget {
    Web,
    FFMpeg
}
