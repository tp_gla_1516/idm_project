package fr.istic.gla.idm.transformation

import java.io.File

class UtilsTransformation {
	static def String computeUrlOut(String urlIn,TypeUrl typeUrl,TypeTarget typeTarget) {
		val strings = urlIn.split("/")
        var String newUrl

		var ext = ""
		var folder = ""
		if(typeUrl.equals(TypeUrl.ConvertVideo)) {
			folder = "ConvertedVideos"
			ext = ".ts"
		}
		else if (typeUrl.equals(TypeUrl.Vignette)){
            folder = "Vignettes"
			ext = ".png"
		}
        if(typeTarget.equals(TypeTarget.FFMpeg)) {
		    for (var i=0; i<strings.size; i++) {
                if(i==0)
                    newUrl = strings.get(i)
                else if(i==strings.size-1)
                    return newUrl += "/"+folder+"/"+strings.get(i).split("\\.").get(0)+""+ext
                else
                    newUrl +="/"+strings.get(i)
            }
        }
        else  if(typeTarget.equals(TypeTarget.Web)) {
            if(typeUrl.equals(TypeUrl.ConvertVideo)) {
                newUrl = "../"
            }
            else if(typeUrl.equals(TypeUrl.Vignette)) {
                newUrl = "resources/"
            }
            return newUrl += "videos/"+folder+"/"+strings.get(strings.size-1).split("\\.").get(0)+""+ext
        }
		return ""
	}

	static def alreadyExist(String url){
		new File(url).exists()
	}
}
