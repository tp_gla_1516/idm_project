package fr.istic.gla.idm.transformation

import fr.istic.gla.idm.videoGen.Mandatory
import fr.istic.gla.idm.videoGen.Optional
import fr.istic.gla.idm.videoGen.Alternatives
import fr.istic.gla.idm.videoGen.Single
import fr.istic.gla.idm.videoGen.Sequence
import org.eclipse.emf.ecore.resource.Resource
import fr.istic.gla.idm.VideoGenStandaloneSetupGenerated
import fr.istic.gla.idm.videoGen.VideoGen
import java.io.FileWriter
import java.util.ArrayList
import java.util.Random
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import playlist.Playlist
import playlist.PlaylistFactory
import fr.istic.gla.idm.videoGen.VideoGenFactory
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser;
import java.util.HashMap

class VideoGenTransformation {

	Random RANDOM
	PlaylistFactory plFactory
	VideoGen videoGen

	enum GeneratorType {
		HTMLVignette
	}
	enum TransformationTarget {
		PlaylistRandom,
		PlaylistNotRandom
	}

	new(String pathIn) {
		videoGen = loadVideoGen(URI.createFileURI(pathIn))
		plFactory = PlaylistFactory.eINSTANCE
		RANDOM = new Random
	}

	def void generator(String pathOut, GeneratorType type){
		val fw = new FileWriter(pathOut);
		if	(type.equals(GeneratorType.HTMLVignette)) generatorHTMLVignette(fw)
		else System.out.println("Type : " + type + " Non géré")
		fw.close();
	}

	def transformator(TransformationTarget target,ArrayList<String> listId){
		val playlist = plFactory.createPlaylist()
		if	(target.equals(TransformationTarget.PlaylistRandom)) toPlaylistRandom(playlist)
		else if	(target.equals(TransformationTarget.PlaylistNotRandom)) toPlaylist(listId,playlist)
		else System.out.println("Target : " + target + " Non géré")
		return playlist;
	}

    private def loadVideoGen(URI uri) {
        new VideoGenStandaloneSetupGenerated().createInjectorAndDoEMFRegistration()
        var res = new ResourceSetImpl().getResource(uri, true);
        res.contents.get(0) as VideoGen
	}

	private def int random(int max) {
        return (RANDOM.nextDouble() * max) as int
    }

	private def propertyCheck(){
		//TODO Tests !!
		true;
	}

	def void toPlaylistRandom(Playlist playlist){
		videoGen.list.forEach[v|
			val video = plFactory.createVideo()
			if(v instanceof Mandatory){
				video.url = v.videoSequences.url
				video.duration =UtilsFfmpeg.getDuration(v.videoSequences.url)
			}
			else if(v instanceof Alternatives){
				//Une chance sur le nombre d'alternative
				val vs = v.videoSequences.get(random(v.videoSequences.size()))
				video.url = vs.url
				video.duration = UtilsFfmpeg.getDuration(vs.url)
				}
			else if(v instanceof Optional){
				if(random(2)==1) { //Une chance sur 2
					video.url = v.videoSequences.url
					video.duration = UtilsFfmpeg.getDuration(v.videoSequences.url)
				}
				else video.url = null;
			}
			else video.url = null;
			if (video.url != null) playlist.videos.add(video)
		]
	}

	def void toPlaylist(ArrayList<String> listId,Playlist playlist){
        videoGen.list.forEach[v|
			val video = plFactory.createVideo()
			if(v instanceof Single){
                if(listId.contains(v.videoSequences.id)) {
                    video.url = v.videoSequences.url
                    video.duration = UtilsFfmpeg.getDuration(v.videoSequences.url)
                }
            }
			else if(v instanceof Alternatives){
				v.videoSequences.forEach[vs|
                    if(listId.contains(vs.id)) {
                        video.url = vs.url
                        video.duration = UtilsFfmpeg.getDuration(vs.url)
                    }
				]
			}
			if (video.url != null && video.duration != null) playlist.videos.add(video)
		]
	}

	def generatorHTMLVignette(FileWriter fw) {
		fw.write('<div ng-controller="chooseVideoCtrl">'+
  '\n\t<uib-alert ng-repeat="alert in alerts" type="{{alert.type}}" close="closeAlert($index)"> {{alert.msg}} </uib-alert>');
		fw.write(formGenerateBtn());
		fw.write('\n\t<uib-accordion close-others="true" class="col-lg-8">');
		var id=0
		for(Sequence v : videoGen.list){
			if(v instanceof Mandatory){
				fw.write('\n\t\t<display-mandatory nom="'+v.videoSequences.nomVideo+'" index="'+id+'" url="'+UtilsTransformation.computeUrlOut(v.videoSequences.url,TypeUrl.Vignette,TypeTarget.Web)+'" id="'+v.videoSequences.id+'"></display-mandatory>');
			}
			else if(v instanceof Optional){
				fw.write('\n\t\t<display-optional nom="'+v.videoSequences.nomVideo+'" index="'+id+'" url="'+UtilsTransformation.computeUrlOut(v.videoSequences.url,TypeUrl.Vignette,TypeTarget.Web)+'" id="'+v.videoSequences.id+'"></display-optional>');
			}
			else if(v instanceof Alternatives){

				fw.write('\n\t\t<display-alternative index="'+id+'" id="'+v.id+'">');
				var index=0
				for(vs : v.videoSequences){
					fw.write('\n\t\t\t<display-alternative-video nom="'+vs.nomVideo+'" index="'+(index)+'" id="'+vs.id+'" url="'+UtilsTransformation.computeUrlOut(vs.url,TypeUrl.Vignette,TypeTarget.Web)+'"></display-alternative-video>');
				}
				fw.write('\n\t\t</display-alternative>');
			}
			id++;
		}
		fw.write(' \n\t</uib-accordion> \n</div> ');
	}
	private def formGenerateBtn(){
		'\n\t<div id="menuGenerate" class="container col-lg-2">\n' +
		'\t\t<button type="button" ng-click="getNSendListIdVideo()" class="btn btn-primary col-lg-12 btn-lg">Generate</button>\n' +
		'\t\t<button type="button" ng-click="getNSendListIdVideo()" class="btn btn-primary col-lg-12 btn-lg">Random generation</button>\n\t</div>'
	}

    static def createNewVideoGen(String urlOut,String stringJson){
        val videoGenFactory = VideoGenFactory.eINSTANCE
        var videoGen = videoGenFactory.createVideoGen();
        var parser = new JSONParser();
        var json = parser.parse(stringJson) as JSONObject
        var i=0
        for(Object s: json.get("sequence") as JSONArray){
            val sJson = s as JSONObject
            val type = sJson.get("type") as String
            if(type.equals("mandatory") || type.equals("optional"))
                videoGen.list.add(createNewSingle(videoGenFactory,i,type,sJson.get("sequence.name") as String,sJson.get("sequence.url") as String))
            i++;
        }
        saveVideoGenInFile(videoGen,urlOut);
    }

    static def Sequence createNewSingle(VideoGenFactory vgF,Integer i,String type,String name, String url){
        var single = vgF.createSingle()
        single.videoSequences = createNewVideoSequence (vgF,"v"+i,name,url)
        if(type.equals("mandatory")) {
            var mandatory = vgF.createMandatory()
            mandatory.videoSequences = createNewVideoSequence (vgF,"v"+i,name,url)
            return mandatory
        }
        else if(type.equals("optional")) {
            var optional = vgF.createOptional()
            optional.videoSequences = createNewVideoSequence(vgF,"v"+i,name,url)
            return optional
        }
    }

    static def Sequence createNewMultiple(VideoGenFactory vgF,Integer i,String name, JSONArray listeSeq){
        var alternative = vgF.createAlternatives()
        alternative.id = "v"+i
        var j=0
        for(Object s: listeSeq){
            val sJson = s as JSONObject
            alternative.videoSequences.add(createNewVideoSequence (vgF,alternative.id+j,sJson.get("name") as String,sJson.get("name") as String))
            j++;
        }
        return alternative
    }

    static def createNewVideoSequence (VideoGenFactory vgF,String id,String name,String url){
        var videoSeq = vgF.createVideoSequence()
        videoSeq.nomVideo = name
        videoSeq.url = url
        videoSeq.id = id
        return videoSeq
    }

    static def saveVideoGenInFile(VideoGen videoGen,String urlOut){
        System.out.println(urlOut)
        System.out.println(videoGen)
        var Ressource rs = new ResourceSetImpl().createResource(URI.createURI(urlOut));
        rs.getContents.add(videoGen);
        rs.save(new HashMap());
    }

}
