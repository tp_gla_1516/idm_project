package fr.istic.gla.idm.transformation;

import fr.istic.gla.idm.transformation.PlaylistTransformation;
import fr.istic.gla.idm.transformation.VideoGenTransformation;
import playlist.Playlist;

@SuppressWarnings("all")
public class RunTransformation {
  public static void main(final String[] args) {
    VideoGenTransformation.createNewVideoGen(
      "/home/lautre/Documents/Cours/IDM/TP/projetIDM/IDMProject_jHipster/src/main/webapp/resources/playlistVideoGen", 
      "{\"name\":\"test\",\"sequence\":[{\"type\":\"alternative\",\"listSequence\":[{\"name\":\"Badgers\",\"url\":\"/home/lautre/Documents/Cours/IDM/TP/projetIDM/IDMProject_jHipster/src/main/webapp/resources/videos/Badgers.mp4\"},{\"name\":\"Narwhals\",\"url\":\"/home/lautre/Documents/Cours/IDM/TP/projetIDM/IDMProject_jHipster/src/main/webapp/resources/videos/Narwhals.mp4\"}]},{\"type\":\"mandatory\",\"sequence\":{\"name\":\"Amazing Horse\",\"url\":\"/home/lautre/Documents/Cours/IDM/TP/projetIDM/IDMProject_jHipster/src/main/webapp/resources/videos/Amazing_Horse.mp4\"}}]}\n");
    VideoGenTransformation vgTrans = new VideoGenTransformation("/home/lautre/Documents/Cours/IDM/TP/projetIDM/IDMProject_jHipster/src/main/webapp/resources/playlistVideoGen/test.videoGen");
    vgTrans.generator("/home/lautre/Documents/Cours/IDM/TP/projetIDM/vignette.html", VideoGenTransformation.GeneratorType.HTMLVignette);
    Playlist _transformator = vgTrans.transformator(VideoGenTransformation.TransformationTarget.PlaylistRandom, null);
    final PlaylistTransformation plTrans = new PlaylistTransformation(_transformator, 
      "test", 
      "/home/lautre/Documents/Cours/IDM/TP/projetIDM/IDMProject_jHipster/src/main/webapp/resources/playlist", 
      "resources/playlist");
    plTrans.generator("/home/lautre/Documents/Cours/IDM/TP/projetIDM/player.html", PlaylistTransformation.GeneratorType.HTMLPlayerPage);
  }
}
