package fr.istic.gla.idm.transformation;

import fr.istic.gla.idm.transformation.TypeTarget;
import fr.istic.gla.idm.transformation.TypeUrl;
import fr.istic.gla.idm.transformation.UtilsTransformation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class UtilsFfmpeg {
  public static boolean convertToTs(final String urlIn) {
    try {
      final String urlOut = UtilsTransformation.computeUrlOut(urlIn, TypeUrl.ConvertVideo, TypeTarget.FFMpeg);
      boolean _alreadyExist = UtilsTransformation.alreadyExist(urlOut);
      boolean _not = (!_alreadyExist);
      if (_not) {
        final Runtime rt = Runtime.getRuntime();
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("ffmpeg -i ");
        _builder.append(urlIn, "");
        _builder.append(" -strict -2 -vcodec libx264 -acodec aac -f mpegts ");
        _builder.append(urlOut, "");
        final List<String> cmd = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("/bin/sh", "-c", _builder.toString()));
        try {
          Process p = rt.exec(((String[])Conversions.unwrapArray(cmd, String.class)));
          p.waitFor();
          return true;
        } catch (final Throwable _t) {
          if (_t instanceof IOException) {
            final IOException e = (IOException)_t;
            e.printStackTrace();
            return false;
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
      }
      return true;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static String createVignette(final String urlIn) {
    try {
      final String urlOut = UtilsTransformation.computeUrlOut(urlIn, TypeUrl.Vignette, TypeTarget.FFMpeg);
      System.out.println(urlOut);
      boolean _alreadyExist = UtilsTransformation.alreadyExist(urlOut);
      boolean _not = (!_alreadyExist);
      if (_not) {
        final Runtime rt = Runtime.getRuntime();
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("ffmpeg -y -i  ");
        _builder.append(urlIn, "");
        _builder.append("  -r 1 -t 00:00:01 -ss 00:00:01 -f image2  ");
        _builder.append(urlOut, "");
        _builder.append(" ");
        final List<String> cmd = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("/bin/sh", "-c", _builder.toString()));
        try {
          Process p = rt.exec(((String[])Conversions.unwrapArray(cmd, String.class)));
          p.waitFor();
        } catch (final Throwable _t) {
          if (_t instanceof IOException) {
            final IOException e = (IOException)_t;
            e.printStackTrace();
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
      }
      return urlOut;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static String getDuration(final String filename) {
    final Runtime rt = Runtime.getRuntime();
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("ffprobe -i ");
    _builder.append(filename, "");
    _builder.append(" -show_format | grep duration");
    final List<String> cmd = Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("/bin/sh", "-c", _builder.toString()));
    try {
      final Process p = rt.exec(((String[])Conversions.unwrapArray(cmd, String.class)));
      InputStream _inputStream = p.getInputStream();
      InputStreamReader _inputStreamReader = new InputStreamReader(_inputStream);
      final BufferedReader stdInput = new BufferedReader(_inputStreamReader);
      final String durationLine = stdInput.readLine();
      String[] _split = durationLine.split("=");
      return _split[1];
    } catch (final Throwable _t) {
      if (_t instanceof IOException) {
        final IOException e = (IOException)_t;
        e.printStackTrace();
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return null;
  }
}
