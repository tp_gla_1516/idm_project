package fr.istic.gla.idm.transformation;

import fr.istic.gla.idm.transformation.TypeTarget;
import fr.istic.gla.idm.transformation.TypeUrl;
import java.io.File;
import java.util.List;
import org.eclipse.xtext.xbase.lib.Conversions;

@SuppressWarnings("all")
public class UtilsTransformation {
  public static String computeUrlOut(final String urlIn, final TypeUrl typeUrl, final TypeTarget typeTarget) {
    final String[] strings = urlIn.split("/");
    String newUrl = null;
    String ext = "";
    String folder = "";
    boolean _equals = typeUrl.equals(TypeUrl.ConvertVideo);
    if (_equals) {
      folder = "ConvertedVideos";
      ext = ".ts";
    } else {
      boolean _equals_1 = typeUrl.equals(TypeUrl.Vignette);
      if (_equals_1) {
        folder = "Vignettes";
        ext = ".png";
      }
    }
    boolean _equals_2 = typeTarget.equals(TypeTarget.FFMpeg);
    if (_equals_2) {
      for (int i = 0; (i < ((List<String>)Conversions.doWrapArray(strings)).size()); i++) {
        if ((i == 0)) {
          String _get = strings[i];
          newUrl = _get;
        } else {
          int _size = ((List<String>)Conversions.doWrapArray(strings)).size();
          int _minus = (_size - 1);
          boolean _equals_3 = (i == _minus);
          if (_equals_3) {
            String _newUrl = newUrl;
            String _get_1 = strings[i];
            String[] _split = _get_1.split("\\.");
            String _get_2 = _split[0];
            String _plus = ((("/" + folder) + "/") + _get_2);
            String _plus_1 = (_plus + "");
            String _plus_2 = (_plus_1 + ext);
            return newUrl = (_newUrl + _plus_2);
          } else {
            String _newUrl_1 = newUrl;
            String _get_3 = strings[i];
            String _plus_3 = ("/" + _get_3);
            newUrl = (_newUrl_1 + _plus_3);
          }
        }
      }
    } else {
      boolean _equals_3 = typeTarget.equals(TypeTarget.Web);
      if (_equals_3) {
        boolean _equals_4 = typeUrl.equals(TypeUrl.ConvertVideo);
        if (_equals_4) {
          newUrl = "../";
        } else {
          boolean _equals_5 = typeUrl.equals(TypeUrl.Vignette);
          if (_equals_5) {
            newUrl = "resources/";
          }
        }
        String _newUrl = newUrl;
        int _size = ((List<String>)Conversions.doWrapArray(strings)).size();
        int _minus = (_size - 1);
        String _get = strings[_minus];
        String[] _split = _get.split("\\.");
        String _get_1 = _split[0];
        String _plus = ((("videos/" + folder) + "/") + _get_1);
        String _plus_1 = (_plus + "");
        String _plus_2 = (_plus_1 + ext);
        return newUrl = (_newUrl + _plus_2);
      }
    }
    return "";
  }
  
  public static boolean alreadyExist(final String url) {
    File _file = new File(url);
    return _file.exists();
  }
}
