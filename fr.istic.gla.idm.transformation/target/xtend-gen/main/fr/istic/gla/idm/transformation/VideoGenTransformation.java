package fr.istic.gla.idm.transformation;

import com.google.common.base.Objects;
import fr.istic.gla.idm.VideoGenStandaloneSetupGenerated;
import fr.istic.gla.idm.transformation.TypeTarget;
import fr.istic.gla.idm.transformation.TypeUrl;
import fr.istic.gla.idm.transformation.UtilsFfmpeg;
import fr.istic.gla.idm.transformation.UtilsTransformation;
import fr.istic.gla.idm.videoGen.Alternatives;
import fr.istic.gla.idm.videoGen.Mandatory;
import fr.istic.gla.idm.videoGen.Optional;
import fr.istic.gla.idm.videoGen.Sequence;
import fr.istic.gla.idm.videoGen.Single;
import fr.istic.gla.idm.videoGen.VideoGen;
import fr.istic.gla.idm.videoGen.VideoGenFactory;
import fr.istic.gla.idm.videoGen.VideoSequence;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import playlist.Playlist;
import playlist.PlaylistFactory;
import playlist.Video;

@SuppressWarnings("all")
public class VideoGenTransformation {
  public enum GeneratorType {
    HTMLVignette;
  }
  
  public enum TransformationTarget {
    PlaylistRandom,
    
    PlaylistNotRandom;
  }
  
  private Random RANDOM;
  
  private PlaylistFactory plFactory;
  
  private VideoGen videoGen;
  
  public VideoGenTransformation(final String pathIn) {
    URI _createFileURI = URI.createFileURI(pathIn);
    VideoGen _loadVideoGen = this.loadVideoGen(_createFileURI);
    this.videoGen = _loadVideoGen;
    boolean _equals = Objects.equal(this.videoGen, null);
    if (_equals) {
      System.out.println("Error de chargement du fichier");
    }
    this.plFactory = PlaylistFactory.eINSTANCE;
    Random _random = new Random();
    this.RANDOM = _random;
  }
  
  public void generator(final String pathOut, final VideoGenTransformation.GeneratorType type) {
    try {
      final FileWriter fw = new FileWriter(pathOut);
      boolean _equals = type.equals(VideoGenTransformation.GeneratorType.HTMLVignette);
      if (_equals) {
        this.generatorHTMLVignette(fw);
      } else {
        System.out.println((("Type : " + type) + " Non géré"));
      }
      fw.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public Playlist transformator(final VideoGenTransformation.TransformationTarget target, final ArrayList<String> listId) {
    final Playlist playlist = this.plFactory.createPlaylist();
    boolean _equals = target.equals(VideoGenTransformation.TransformationTarget.PlaylistRandom);
    if (_equals) {
      this.toPlaylistRandom(playlist);
    } else {
      boolean _equals_1 = target.equals(VideoGenTransformation.TransformationTarget.PlaylistNotRandom);
      if (_equals_1) {
        this.toPlaylist(listId, playlist);
      } else {
        System.out.println((("Target : " + target) + " Non géré"));
      }
    }
    return playlist;
  }
  
  private VideoGen loadVideoGen(final URI uri) {
    VideoGen _xblockexpression = null;
    {
      VideoGenStandaloneSetupGenerated _videoGenStandaloneSetupGenerated = new VideoGenStandaloneSetupGenerated();
      _videoGenStandaloneSetupGenerated.createInjectorAndDoEMFRegistration();
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource res = _resourceSetImpl.getResource(uri, true);
      EList<EObject> _contents = res.getContents();
      EObject _get = _contents.get(0);
      final VideoGen videoGen = ((VideoGen) _get);
      VideoGen _xifexpression = null;
      boolean _propertyCheck = this.propertyCheck(videoGen);
      if (_propertyCheck) {
        _xifexpression = videoGen;
      } else {
        _xifexpression = null;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  private int random(final int max) {
    double _nextDouble = this.RANDOM.nextDouble();
    double _multiply = (_nextDouble * max);
    return ((int) _multiply);
  }
  
  private boolean propertyCheck(final VideoGen videoGen) {
    final ArrayList<String> listID = CollectionLiterals.<String>newArrayList();
    EList<Sequence> _list = videoGen.getList();
    for (final Sequence s : _list) {
      if ((s instanceof Alternatives)) {
        EList<VideoSequence> _videoSequences = ((Alternatives)s).getVideoSequences();
        int _length = ((Object[])Conversions.unwrapArray(_videoSequences, Object.class)).length;
        boolean _lessEqualsThan = (_length <= 1);
        if (_lessEqualsThan) {
          System.out.println("taille Alternatives egal ou inférireur a 1");
          return false;
        }
        EList<VideoSequence> _videoSequences_1 = ((Alternatives)s).getVideoSequences();
        for (final VideoSequence vs : _videoSequences_1) {
          {
            String _url = vs.getUrl();
            final File file = new File(_url);
            String _id = vs.getId();
            boolean _contains = listID.contains(_id);
            if (_contains) {
              System.out.println("Id non unique");
              return false;
            } else {
              boolean _exists = file.exists();
              boolean _not = (!_exists);
              if (_not) {
                System.out.println("Le fichier n\'existe pas\'");
                return false;
              } else {
                String _id_1 = vs.getId();
                listID.add(_id_1);
              }
            }
          }
        }
      } else {
        if ((s instanceof Single)) {
          VideoSequence _videoSequences_2 = ((Single)s).getVideoSequences();
          String _url = _videoSequences_2.getUrl();
          final File file = new File(_url);
          VideoSequence _videoSequences_3 = ((Single)s).getVideoSequences();
          String _id = _videoSequences_3.getId();
          boolean _contains = listID.contains(_id);
          if (_contains) {
            System.out.println("Id non unique");
            return false;
          } else {
            boolean _exists = file.exists();
            boolean _not = (!_exists);
            if (_not) {
              System.out.println("Le fichier n\'existe pas\'");
              return false;
            } else {
              VideoSequence _videoSequences_4 = ((Single)s).getVideoSequences();
              String _id_1 = _videoSequences_4.getId();
              listID.add(_id_1);
            }
          }
        }
      }
    }
    return true;
  }
  
  public void toPlaylistRandom(final Playlist playlist) {
    EList<Sequence> _list = this.videoGen.getList();
    final Consumer<Sequence> _function = new Consumer<Sequence>() {
      @Override
      public void accept(final Sequence v) {
        final Video video = VideoGenTransformation.this.plFactory.createVideo();
        if ((v instanceof Mandatory)) {
          VideoSequence _videoSequences = ((Mandatory)v).getVideoSequences();
          String _url = _videoSequences.getUrl();
          video.setUrl(_url);
          VideoSequence _videoSequences_1 = ((Mandatory)v).getVideoSequences();
          String _url_1 = _videoSequences_1.getUrl();
          String _duration = UtilsFfmpeg.getDuration(_url_1);
          video.setDuration(_duration);
        } else {
          if ((v instanceof Alternatives)) {
            EList<VideoSequence> _videoSequences_2 = ((Alternatives)v).getVideoSequences();
            EList<VideoSequence> _videoSequences_3 = ((Alternatives)v).getVideoSequences();
            int _size = _videoSequences_3.size();
            int _random = VideoGenTransformation.this.random(_size);
            final VideoSequence vs = _videoSequences_2.get(_random);
            String _url_2 = vs.getUrl();
            video.setUrl(_url_2);
            String _url_3 = vs.getUrl();
            String _duration_1 = UtilsFfmpeg.getDuration(_url_3);
            video.setDuration(_duration_1);
          } else {
            if ((v instanceof Optional)) {
              int _random_1 = VideoGenTransformation.this.random(2);
              boolean _equals = (_random_1 == 1);
              if (_equals) {
                VideoSequence _videoSequences_4 = ((Optional)v).getVideoSequences();
                String _url_4 = _videoSequences_4.getUrl();
                video.setUrl(_url_4);
                VideoSequence _videoSequences_5 = ((Optional)v).getVideoSequences();
                String _url_5 = _videoSequences_5.getUrl();
                String _duration_2 = UtilsFfmpeg.getDuration(_url_5);
                video.setDuration(_duration_2);
              } else {
                video.setUrl(null);
              }
            } else {
              video.setUrl(null);
            }
          }
        }
        String _url_6 = video.getUrl();
        boolean _notEquals = (!Objects.equal(_url_6, null));
        if (_notEquals) {
          EList<Video> _videos = playlist.getVideos();
          _videos.add(video);
        }
      }
    };
    _list.forEach(_function);
  }
  
  public void toPlaylist(final ArrayList<String> listId, final Playlist playlist) {
    EList<Sequence> _list = this.videoGen.getList();
    final Consumer<Sequence> _function = new Consumer<Sequence>() {
      @Override
      public void accept(final Sequence v) {
        final Video video = VideoGenTransformation.this.plFactory.createVideo();
        if ((v instanceof Single)) {
          VideoSequence _videoSequences = ((Single)v).getVideoSequences();
          String _id = _videoSequences.getId();
          boolean _contains = listId.contains(_id);
          if (_contains) {
            VideoSequence _videoSequences_1 = ((Single)v).getVideoSequences();
            String _url = _videoSequences_1.getUrl();
            video.setUrl(_url);
            VideoSequence _videoSequences_2 = ((Single)v).getVideoSequences();
            String _url_1 = _videoSequences_2.getUrl();
            String _duration = UtilsFfmpeg.getDuration(_url_1);
            video.setDuration(_duration);
          }
        } else {
          if ((v instanceof Alternatives)) {
            EList<VideoSequence> _videoSequences_3 = ((Alternatives)v).getVideoSequences();
            final Consumer<VideoSequence> _function = new Consumer<VideoSequence>() {
              @Override
              public void accept(final VideoSequence vs) {
                String _id = vs.getId();
                boolean _contains = listId.contains(_id);
                if (_contains) {
                  String _url = vs.getUrl();
                  video.setUrl(_url);
                  String _url_1 = vs.getUrl();
                  String _duration = UtilsFfmpeg.getDuration(_url_1);
                  video.setDuration(_duration);
                }
              }
            };
            _videoSequences_3.forEach(_function);
          }
        }
        boolean _and = false;
        String _url_2 = video.getUrl();
        boolean _notEquals = (!Objects.equal(_url_2, null));
        if (!_notEquals) {
          _and = false;
        } else {
          String _duration_1 = video.getDuration();
          boolean _notEquals_1 = (!Objects.equal(_duration_1, null));
          _and = _notEquals_1;
        }
        if (_and) {
          EList<Video> _videos = playlist.getVideos();
          _videos.add(video);
        }
      }
    };
    _list.forEach(_function);
  }
  
  public void generatorHTMLVignette(final FileWriter fw) {
    try {
      fw.write(
        ("<div ng-controller=\"chooseVideoCtrl\">" + 
          "\n\t<uib-alert ng-repeat=\"alert in alerts\" type=\"{{alert.type}}\" close=\"closeAlert($index)\"> {{alert.msg}} </uib-alert>"));
      String _formGenerateBtn = this.formGenerateBtn();
      fw.write(_formGenerateBtn);
      fw.write("\n\t<uib-accordion close-others=\"true\" class=\"col-lg-8\">");
      int id = 0;
      EList<Sequence> _list = this.videoGen.getList();
      for (final Sequence v : _list) {
        {
          if ((v instanceof Mandatory)) {
            VideoSequence _videoSequences = ((Mandatory)v).getVideoSequences();
            String _url = _videoSequences.getUrl();
            String _createVignette = UtilsFfmpeg.createVignette(_url);
            fw.write(_createVignette);
            VideoSequence _videoSequences_1 = ((Mandatory)v).getVideoSequences();
            String _nomVideo = _videoSequences_1.getNomVideo();
            String _plus = ("\n\t\t<display-mandatory nom=\"" + _nomVideo);
            String _plus_1 = (_plus + "\" index=\"");
            String _plus_2 = (_plus_1 + Integer.valueOf(id));
            String _plus_3 = (_plus_2 + "\" url=\"");
            VideoSequence _videoSequences_2 = ((Mandatory)v).getVideoSequences();
            String _url_1 = _videoSequences_2.getUrl();
            String _computeUrlOut = UtilsTransformation.computeUrlOut(_url_1, TypeUrl.Vignette, TypeTarget.Web);
            String _plus_4 = (_plus_3 + _computeUrlOut);
            String _plus_5 = (_plus_4 + "\" id=\"");
            VideoSequence _videoSequences_3 = ((Mandatory)v).getVideoSequences();
            String _id = _videoSequences_3.getId();
            String _plus_6 = (_plus_5 + _id);
            String _plus_7 = (_plus_6 + "\"></display-mandatory>");
            fw.write(_plus_7);
          } else {
            if ((v instanceof Optional)) {
              VideoSequence _videoSequences_4 = ((Optional)v).getVideoSequences();
              String _url_2 = _videoSequences_4.getUrl();
              String _createVignette_1 = UtilsFfmpeg.createVignette(_url_2);
              fw.write(_createVignette_1);
              VideoSequence _videoSequences_5 = ((Optional)v).getVideoSequences();
              String _nomVideo_1 = _videoSequences_5.getNomVideo();
              String _plus_8 = ("\n\t\t<display-optional nom=\"" + _nomVideo_1);
              String _plus_9 = (_plus_8 + "\" index=\"");
              String _plus_10 = (_plus_9 + Integer.valueOf(id));
              String _plus_11 = (_plus_10 + "\" url=\"");
              VideoSequence _videoSequences_6 = ((Optional)v).getVideoSequences();
              String _url_3 = _videoSequences_6.getUrl();
              String _computeUrlOut_1 = UtilsTransformation.computeUrlOut(_url_3, TypeUrl.Vignette, TypeTarget.Web);
              String _plus_12 = (_plus_11 + _computeUrlOut_1);
              String _plus_13 = (_plus_12 + "\" id=\"");
              VideoSequence _videoSequences_7 = ((Optional)v).getVideoSequences();
              String _id_1 = _videoSequences_7.getId();
              String _plus_14 = (_plus_13 + _id_1);
              String _plus_15 = (_plus_14 + "\"></display-optional>");
              fw.write(_plus_15);
            } else {
              if ((v instanceof Alternatives)) {
                String _id_2 = ((Alternatives)v).getId();
                String _plus_16 = ((("\n\t\t<display-alternative index=\"" + Integer.valueOf(id)) + "\" id=\"") + _id_2);
                String _plus_17 = (_plus_16 + "\">");
                fw.write(_plus_17);
                int index = 0;
                EList<VideoSequence> _videoSequences_8 = ((Alternatives)v).getVideoSequences();
                for (final VideoSequence vs : _videoSequences_8) {
                  {
                    String _url_4 = vs.getUrl();
                    UtilsFfmpeg.createVignette(_url_4);
                    String _nomVideo_2 = vs.getNomVideo();
                    String _plus_18 = ("\n\t\t\t<display-alternative-video nom=\"" + _nomVideo_2);
                    String _plus_19 = (_plus_18 + "\" index=\"");
                    String _plus_20 = (_plus_19 + Integer.valueOf(index));
                    String _plus_21 = (_plus_20 + "\" id=\"");
                    String _id_3 = vs.getId();
                    String _plus_22 = (_plus_21 + _id_3);
                    String _plus_23 = (_plus_22 + "\" url=\"");
                    String _url_5 = vs.getUrl();
                    String _computeUrlOut_2 = UtilsTransformation.computeUrlOut(_url_5, TypeUrl.Vignette, TypeTarget.Web);
                    String _plus_24 = (_plus_23 + _computeUrlOut_2);
                    String _plus_25 = (_plus_24 + "\"></display-alternative-video>");
                    fw.write(_plus_25);
                  }
                }
                fw.write("\n\t\t</display-alternative>");
              }
            }
          }
          id++;
        }
      }
      fw.write(" \n\t</uib-accordion> \n</div> ");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private String formGenerateBtn() {
    return (((((("\n\t<div id=\"menuGenerate\" class=\"container col-lg-2\">\n" + 
      "\t\t<a href=\"#/\" role=\"button\" class=\"btn-success btn col-lg-12 btn-lg\">\n") + 
      "\t\t\t<span class=\"glyphicon-home glyphicon\"></span> Home\n") + 
      "\t\t</a>\n") + 
      "\t\t<button type=\"button\" ng-click=\"getNSendListIdVideo()\" class=\"btn btn-primary col-lg-12 btn-lg\">Generate</button>\n") + 
      "\t\t<button type=\"button\" ng-click=\"playlistRandom()\" class=\"btn btn-primary col-lg-12 btn-lg\">Random generation</button>\n") + 
      "\t</div>");
  }
  
  public static void createNewVideoGen(final String urlOut, final String stringJson) {
    try {
      final VideoGenFactory videoGenFactory = VideoGenFactory.eINSTANCE;
      VideoGen videoGen = videoGenFactory.createVideoGen();
      JSONParser parser = new JSONParser();
      Object _parse = parser.parse(stringJson);
      JSONObject json = ((JSONObject) _parse);
      int i = 1;
      Object _get = json.get("sequence");
      for (final Object s : ((JSONArray) _get)) {
        {
          final JSONObject sJson = ((JSONObject) s);
          Object _get_1 = sJson.get("type");
          final String type = ((String) _get_1);
          boolean _or = false;
          boolean _equals = type.equals("mandatory");
          if (_equals) {
            _or = true;
          } else {
            boolean _equals_1 = type.equals("optional");
            _or = _equals_1;
          }
          if (_or) {
            Object _get_2 = sJson.get("sequence");
            JSONObject sequence = ((JSONObject) _get_2);
            EList<Sequence> _list = videoGen.getList();
            Object _get_3 = sequence.get("name");
            Object _get_4 = sequence.get("url");
            Sequence _createNewSingle = VideoGenTransformation.createNewSingle(videoGenFactory, Integer.valueOf(i), type, ((String) _get_3), ((String) _get_4));
            _list.add(_createNewSingle);
          } else {
            boolean _equals_2 = type.equals("alternative");
            if (_equals_2) {
              EList<Sequence> _list_1 = videoGen.getList();
              Object _get_5 = sJson.get("listSequence");
              Sequence _createNewMultiple = VideoGenTransformation.createNewMultiple(videoGenFactory, Integer.valueOf(i), ((JSONArray) _get_5));
              _list_1.add(_createNewMultiple);
            }
          }
          i++;
        }
      }
      Object _get_1 = json.get("name");
      String name = ((String) _get_1);
      String _replace = name.replace(" ", "_");
      name = _replace;
      URI _createURI = URI.createURI((((urlOut + "/") + name) + ".videoGen"));
      VideoGenTransformation.saveVideoGenInFile(videoGen, _createURI);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static Sequence createNewSingle(final VideoGenFactory vgF, final Integer i, final String type, final String name, final String url) {
    boolean _equals = type.equals("mandatory");
    if (_equals) {
      Mandatory mandatory = vgF.createMandatory();
      VideoSequence _createNewVideoSequence = VideoGenTransformation.createNewVideoSequence(vgF, ("v" + i), name, url);
      mandatory.setVideoSequences(_createNewVideoSequence);
      return mandatory;
    } else {
      boolean _equals_1 = type.equals("optional");
      if (_equals_1) {
        Optional optional = vgF.createOptional();
        VideoSequence _createNewVideoSequence_1 = VideoGenTransformation.createNewVideoSequence(vgF, ("v" + i), name, url);
        optional.setVideoSequences(_createNewVideoSequence_1);
        return optional;
      }
    }
    return null;
  }
  
  public static Sequence createNewMultiple(final VideoGenFactory vgF, final Integer i, final JSONArray listeSeq) {
    Alternatives alternative = vgF.createAlternatives();
    alternative.setId(("v" + i));
    int j = 1;
    for (final Object s : listeSeq) {
      {
        final JSONObject sJson = ((JSONObject) s);
        EList<VideoSequence> _videoSequences = alternative.getVideoSequences();
        String _id = alternative.getId();
        String _plus = (_id + Integer.valueOf(j));
        Object _get = sJson.get("name");
        Object _get_1 = sJson.get("url");
        VideoSequence _createNewVideoSequence = VideoGenTransformation.createNewVideoSequence(vgF, _plus, ((String) _get), ((String) _get_1));
        _videoSequences.add(_createNewVideoSequence);
        j++;
      }
    }
    return alternative;
  }
  
  public static VideoSequence createNewVideoSequence(final VideoGenFactory vgF, final String id, final String name, final String url) {
    VideoSequence videoSeq = vgF.createVideoSequence();
    videoSeq.setNomVideo(name);
    videoSeq.setUrl(url);
    videoSeq.setId(id);
    return videoSeq;
  }
  
  public static void saveVideoGenInFile(final VideoGen videoGen, final URI uri) {
    try {
      VideoGenStandaloneSetupGenerated _videoGenStandaloneSetupGenerated = new VideoGenStandaloneSetupGenerated();
      _videoGenStandaloneSetupGenerated.createInjectorAndDoEMFRegistration();
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource rs = _resourceSetImpl.createResource(uri);
      EList<EObject> _contents = rs.getContents();
      _contents.add(videoGen);
      HashMap<Object, Object> _hashMap = new HashMap<Object, Object>();
      rs.save(_hashMap);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
