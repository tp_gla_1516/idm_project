package fr.istic.gla.idm.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import fr.istic.gla.idm.services.VideoGenGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalVideoGenParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'VideoGen'", "'{'", "'}'", "'mandatory'", "'optional'", "'videoseq'", "'duration='", "'nomVideo='", "'description='", "'url='", "'alternative'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalVideoGenParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalVideoGenParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalVideoGenParser.tokenNames; }
    public String getGrammarFileName() { return "../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g"; }


     
     	private VideoGenGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(VideoGenGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleVideoGen"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:60:1: entryRuleVideoGen : ruleVideoGen EOF ;
    public final void entryRuleVideoGen() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:61:1: ( ruleVideoGen EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:62:1: ruleVideoGen EOF
            {
             before(grammarAccess.getVideoGenRule()); 
            pushFollow(FOLLOW_ruleVideoGen_in_entryRuleVideoGen61);
            ruleVideoGen();

            state._fsp--;

             after(grammarAccess.getVideoGenRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoGen68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideoGen"


    // $ANTLR start "ruleVideoGen"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:69:1: ruleVideoGen : ( ( rule__VideoGen__Group__0 ) ) ;
    public final void ruleVideoGen() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:73:2: ( ( ( rule__VideoGen__Group__0 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:74:1: ( ( rule__VideoGen__Group__0 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:74:1: ( ( rule__VideoGen__Group__0 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:75:1: ( rule__VideoGen__Group__0 )
            {
             before(grammarAccess.getVideoGenAccess().getGroup()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:76:1: ( rule__VideoGen__Group__0 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:76:2: rule__VideoGen__Group__0
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__0_in_ruleVideoGen94);
            rule__VideoGen__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoGenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideoGen"


    // $ANTLR start "entryRuleSequence"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:88:1: entryRuleSequence : ruleSequence EOF ;
    public final void entryRuleSequence() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:89:1: ( ruleSequence EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:90:1: ruleSequence EOF
            {
             before(grammarAccess.getSequenceRule()); 
            pushFollow(FOLLOW_ruleSequence_in_entryRuleSequence121);
            ruleSequence();

            state._fsp--;

             after(grammarAccess.getSequenceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSequence128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSequence"


    // $ANTLR start "ruleSequence"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:97:1: ruleSequence : ( ( rule__Sequence__Alternatives ) ) ;
    public final void ruleSequence() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:101:2: ( ( ( rule__Sequence__Alternatives ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:102:1: ( ( rule__Sequence__Alternatives ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:102:1: ( ( rule__Sequence__Alternatives ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:103:1: ( rule__Sequence__Alternatives )
            {
             before(grammarAccess.getSequenceAccess().getAlternatives()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:104:1: ( rule__Sequence__Alternatives )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:104:2: rule__Sequence__Alternatives
            {
            pushFollow(FOLLOW_rule__Sequence__Alternatives_in_ruleSequence154);
            rule__Sequence__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSequenceAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSequence"


    // $ANTLR start "entryRuleSingle"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:116:1: entryRuleSingle : ruleSingle EOF ;
    public final void entryRuleSingle() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:117:1: ( ruleSingle EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:118:1: ruleSingle EOF
            {
             before(grammarAccess.getSingleRule()); 
            pushFollow(FOLLOW_ruleSingle_in_entryRuleSingle181);
            ruleSingle();

            state._fsp--;

             after(grammarAccess.getSingleRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSingle188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingle"


    // $ANTLR start "ruleSingle"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:125:1: ruleSingle : ( ( rule__Single__Alternatives ) ) ;
    public final void ruleSingle() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:129:2: ( ( ( rule__Single__Alternatives ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:130:1: ( ( rule__Single__Alternatives ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:130:1: ( ( rule__Single__Alternatives ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:131:1: ( rule__Single__Alternatives )
            {
             before(grammarAccess.getSingleAccess().getAlternatives()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:132:1: ( rule__Single__Alternatives )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:132:2: rule__Single__Alternatives
            {
            pushFollow(FOLLOW_rule__Single__Alternatives_in_ruleSingle214);
            rule__Single__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSingleAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingle"


    // $ANTLR start "entryRuleMultiple"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:144:1: entryRuleMultiple : ruleMultiple EOF ;
    public final void entryRuleMultiple() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:145:1: ( ruleMultiple EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:146:1: ruleMultiple EOF
            {
             before(grammarAccess.getMultipleRule()); 
            pushFollow(FOLLOW_ruleMultiple_in_entryRuleMultiple241);
            ruleMultiple();

            state._fsp--;

             after(grammarAccess.getMultipleRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMultiple248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiple"


    // $ANTLR start "ruleMultiple"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:153:1: ruleMultiple : ( ruleAlternatives ) ;
    public final void ruleMultiple() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:157:2: ( ( ruleAlternatives ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:158:1: ( ruleAlternatives )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:158:1: ( ruleAlternatives )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:159:1: ruleAlternatives
            {
             before(grammarAccess.getMultipleAccess().getAlternativesParserRuleCall()); 
            pushFollow(FOLLOW_ruleAlternatives_in_ruleMultiple274);
            ruleAlternatives();

            state._fsp--;

             after(grammarAccess.getMultipleAccess().getAlternativesParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiple"


    // $ANTLR start "entryRuleMandatory"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:172:1: entryRuleMandatory : ruleMandatory EOF ;
    public final void entryRuleMandatory() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:173:1: ( ruleMandatory EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:174:1: ruleMandatory EOF
            {
             before(grammarAccess.getMandatoryRule()); 
            pushFollow(FOLLOW_ruleMandatory_in_entryRuleMandatory300);
            ruleMandatory();

            state._fsp--;

             after(grammarAccess.getMandatoryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMandatory307); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMandatory"


    // $ANTLR start "ruleMandatory"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:181:1: ruleMandatory : ( ( rule__Mandatory__Group__0 ) ) ;
    public final void ruleMandatory() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:185:2: ( ( ( rule__Mandatory__Group__0 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:186:1: ( ( rule__Mandatory__Group__0 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:186:1: ( ( rule__Mandatory__Group__0 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:187:1: ( rule__Mandatory__Group__0 )
            {
             before(grammarAccess.getMandatoryAccess().getGroup()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:188:1: ( rule__Mandatory__Group__0 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:188:2: rule__Mandatory__Group__0
            {
            pushFollow(FOLLOW_rule__Mandatory__Group__0_in_ruleMandatory333);
            rule__Mandatory__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMandatoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMandatory"


    // $ANTLR start "entryRuleOptional"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:200:1: entryRuleOptional : ruleOptional EOF ;
    public final void entryRuleOptional() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:201:1: ( ruleOptional EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:202:1: ruleOptional EOF
            {
             before(grammarAccess.getOptionalRule()); 
            pushFollow(FOLLOW_ruleOptional_in_entryRuleOptional360);
            ruleOptional();

            state._fsp--;

             after(grammarAccess.getOptionalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOptional367); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptional"


    // $ANTLR start "ruleOptional"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:209:1: ruleOptional : ( ( rule__Optional__Group__0 ) ) ;
    public final void ruleOptional() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:213:2: ( ( ( rule__Optional__Group__0 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:214:1: ( ( rule__Optional__Group__0 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:214:1: ( ( rule__Optional__Group__0 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:215:1: ( rule__Optional__Group__0 )
            {
             before(grammarAccess.getOptionalAccess().getGroup()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:216:1: ( rule__Optional__Group__0 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:216:2: rule__Optional__Group__0
            {
            pushFollow(FOLLOW_rule__Optional__Group__0_in_ruleOptional393);
            rule__Optional__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptional"


    // $ANTLR start "entryRuleVideoSequence"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:228:1: entryRuleVideoSequence : ruleVideoSequence EOF ;
    public final void entryRuleVideoSequence() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:229:1: ( ruleVideoSequence EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:230:1: ruleVideoSequence EOF
            {
             before(grammarAccess.getVideoSequenceRule()); 
            pushFollow(FOLLOW_ruleVideoSequence_in_entryRuleVideoSequence420);
            ruleVideoSequence();

            state._fsp--;

             after(grammarAccess.getVideoSequenceRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoSequence427); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideoSequence"


    // $ANTLR start "ruleVideoSequence"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:237:1: ruleVideoSequence : ( ( rule__VideoSequence__Group__0 ) ) ;
    public final void ruleVideoSequence() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:241:2: ( ( ( rule__VideoSequence__Group__0 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:242:1: ( ( rule__VideoSequence__Group__0 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:242:1: ( ( rule__VideoSequence__Group__0 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:243:1: ( rule__VideoSequence__Group__0 )
            {
             before(grammarAccess.getVideoSequenceAccess().getGroup()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:244:1: ( rule__VideoSequence__Group__0 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:244:2: rule__VideoSequence__Group__0
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__0_in_ruleVideoSequence453);
            rule__VideoSequence__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideoSequence"


    // $ANTLR start "entryRuleAlternatives"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:256:1: entryRuleAlternatives : ruleAlternatives EOF ;
    public final void entryRuleAlternatives() throws RecognitionException {
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:257:1: ( ruleAlternatives EOF )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:258:1: ruleAlternatives EOF
            {
             before(grammarAccess.getAlternativesRule()); 
            pushFollow(FOLLOW_ruleAlternatives_in_entryRuleAlternatives480);
            ruleAlternatives();

            state._fsp--;

             after(grammarAccess.getAlternativesRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAlternatives487); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAlternatives"


    // $ANTLR start "ruleAlternatives"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:265:1: ruleAlternatives : ( ( rule__Alternatives__Group__0 ) ) ;
    public final void ruleAlternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:269:2: ( ( ( rule__Alternatives__Group__0 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:270:1: ( ( rule__Alternatives__Group__0 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:270:1: ( ( rule__Alternatives__Group__0 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:271:1: ( rule__Alternatives__Group__0 )
            {
             before(grammarAccess.getAlternativesAccess().getGroup()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:272:1: ( rule__Alternatives__Group__0 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:272:2: rule__Alternatives__Group__0
            {
            pushFollow(FOLLOW_rule__Alternatives__Group__0_in_ruleAlternatives513);
            rule__Alternatives__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAlternativesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAlternatives"


    // $ANTLR start "rule__Sequence__Alternatives"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:284:1: rule__Sequence__Alternatives : ( ( ruleSingle ) | ( ruleMultiple ) );
    public final void rule__Sequence__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:288:1: ( ( ruleSingle ) | ( ruleMultiple ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=14 && LA1_0<=15)) ) {
                alt1=1;
            }
            else if ( (LA1_0==21) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:289:1: ( ruleSingle )
                    {
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:289:1: ( ruleSingle )
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:290:1: ruleSingle
                    {
                     before(grammarAccess.getSequenceAccess().getSingleParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleSingle_in_rule__Sequence__Alternatives549);
                    ruleSingle();

                    state._fsp--;

                     after(grammarAccess.getSequenceAccess().getSingleParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:295:6: ( ruleMultiple )
                    {
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:295:6: ( ruleMultiple )
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:296:1: ruleMultiple
                    {
                     before(grammarAccess.getSequenceAccess().getMultipleParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleMultiple_in_rule__Sequence__Alternatives566);
                    ruleMultiple();

                    state._fsp--;

                     after(grammarAccess.getSequenceAccess().getMultipleParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sequence__Alternatives"


    // $ANTLR start "rule__Single__Alternatives"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:306:1: rule__Single__Alternatives : ( ( ruleMandatory ) | ( ruleOptional ) );
    public final void rule__Single__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:310:1: ( ( ruleMandatory ) | ( ruleOptional ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            else if ( (LA2_0==15) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:311:1: ( ruleMandatory )
                    {
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:311:1: ( ruleMandatory )
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:312:1: ruleMandatory
                    {
                     before(grammarAccess.getSingleAccess().getMandatoryParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleMandatory_in_rule__Single__Alternatives598);
                    ruleMandatory();

                    state._fsp--;

                     after(grammarAccess.getSingleAccess().getMandatoryParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:317:6: ( ruleOptional )
                    {
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:317:6: ( ruleOptional )
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:318:1: ruleOptional
                    {
                     before(grammarAccess.getSingleAccess().getOptionalParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleOptional_in_rule__Single__Alternatives615);
                    ruleOptional();

                    state._fsp--;

                     after(grammarAccess.getSingleAccess().getOptionalParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Single__Alternatives"


    // $ANTLR start "rule__VideoGen__Group__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:330:1: rule__VideoGen__Group__0 : rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1 ;
    public final void rule__VideoGen__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:334:1: ( rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:335:2: rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__0__Impl_in_rule__VideoGen__Group__0645);
            rule__VideoGen__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__1_in_rule__VideoGen__Group__0648);
            rule__VideoGen__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__0"


    // $ANTLR start "rule__VideoGen__Group__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:342:1: rule__VideoGen__Group__0__Impl : ( () ) ;
    public final void rule__VideoGen__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:346:1: ( ( () ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:347:1: ( () )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:347:1: ( () )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:348:1: ()
            {
             before(grammarAccess.getVideoGenAccess().getVideoGenAction_0()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:349:1: ()
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:351:1: 
            {
            }

             after(grammarAccess.getVideoGenAccess().getVideoGenAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__0__Impl"


    // $ANTLR start "rule__VideoGen__Group__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:361:1: rule__VideoGen__Group__1 : rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2 ;
    public final void rule__VideoGen__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:365:1: ( rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:366:2: rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__1__Impl_in_rule__VideoGen__Group__1706);
            rule__VideoGen__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__2_in_rule__VideoGen__Group__1709);
            rule__VideoGen__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__1"


    // $ANTLR start "rule__VideoGen__Group__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:373:1: rule__VideoGen__Group__1__Impl : ( 'VideoGen' ) ;
    public final void rule__VideoGen__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:377:1: ( ( 'VideoGen' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:378:1: ( 'VideoGen' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:378:1: ( 'VideoGen' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:379:1: 'VideoGen'
            {
             before(grammarAccess.getVideoGenAccess().getVideoGenKeyword_1()); 
            match(input,11,FOLLOW_11_in_rule__VideoGen__Group__1__Impl737); 
             after(grammarAccess.getVideoGenAccess().getVideoGenKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__1__Impl"


    // $ANTLR start "rule__VideoGen__Group__2"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:392:1: rule__VideoGen__Group__2 : rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3 ;
    public final void rule__VideoGen__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:396:1: ( rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:397:2: rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__2__Impl_in_rule__VideoGen__Group__2768);
            rule__VideoGen__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__3_in_rule__VideoGen__Group__2771);
            rule__VideoGen__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__2"


    // $ANTLR start "rule__VideoGen__Group__2__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:404:1: rule__VideoGen__Group__2__Impl : ( '{' ) ;
    public final void rule__VideoGen__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:408:1: ( ( '{' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:409:1: ( '{' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:409:1: ( '{' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:410:1: '{'
            {
             before(grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__VideoGen__Group__2__Impl799); 
             after(grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__2__Impl"


    // $ANTLR start "rule__VideoGen__Group__3"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:423:1: rule__VideoGen__Group__3 : rule__VideoGen__Group__3__Impl rule__VideoGen__Group__4 ;
    public final void rule__VideoGen__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:427:1: ( rule__VideoGen__Group__3__Impl rule__VideoGen__Group__4 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:428:2: rule__VideoGen__Group__3__Impl rule__VideoGen__Group__4
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__3__Impl_in_rule__VideoGen__Group__3830);
            rule__VideoGen__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__4_in_rule__VideoGen__Group__3833);
            rule__VideoGen__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__3"


    // $ANTLR start "rule__VideoGen__Group__3__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:435:1: rule__VideoGen__Group__3__Impl : ( ( rule__VideoGen__ListAssignment_3 )* ) ;
    public final void rule__VideoGen__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:439:1: ( ( ( rule__VideoGen__ListAssignment_3 )* ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:440:1: ( ( rule__VideoGen__ListAssignment_3 )* )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:440:1: ( ( rule__VideoGen__ListAssignment_3 )* )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:441:1: ( rule__VideoGen__ListAssignment_3 )*
            {
             before(grammarAccess.getVideoGenAccess().getListAssignment_3()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:442:1: ( rule__VideoGen__ListAssignment_3 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=14 && LA3_0<=15)||LA3_0==21) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:442:2: rule__VideoGen__ListAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__VideoGen__ListAssignment_3_in_rule__VideoGen__Group__3__Impl860);
            	    rule__VideoGen__ListAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getVideoGenAccess().getListAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__3__Impl"


    // $ANTLR start "rule__VideoGen__Group__4"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:452:1: rule__VideoGen__Group__4 : rule__VideoGen__Group__4__Impl ;
    public final void rule__VideoGen__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:456:1: ( rule__VideoGen__Group__4__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:457:2: rule__VideoGen__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__4__Impl_in_rule__VideoGen__Group__4891);
            rule__VideoGen__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__4"


    // $ANTLR start "rule__VideoGen__Group__4__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:463:1: rule__VideoGen__Group__4__Impl : ( '}' ) ;
    public final void rule__VideoGen__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:467:1: ( ( '}' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:468:1: ( '}' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:468:1: ( '}' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:469:1: '}'
            {
             before(grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_4()); 
            match(input,13,FOLLOW_13_in_rule__VideoGen__Group__4__Impl919); 
             after(grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__4__Impl"


    // $ANTLR start "rule__Mandatory__Group__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:492:1: rule__Mandatory__Group__0 : rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1 ;
    public final void rule__Mandatory__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:496:1: ( rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:497:2: rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1
            {
            pushFollow(FOLLOW_rule__Mandatory__Group__0__Impl_in_rule__Mandatory__Group__0960);
            rule__Mandatory__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mandatory__Group__1_in_rule__Mandatory__Group__0963);
            rule__Mandatory__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__0"


    // $ANTLR start "rule__Mandatory__Group__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:504:1: rule__Mandatory__Group__0__Impl : ( 'mandatory' ) ;
    public final void rule__Mandatory__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:508:1: ( ( 'mandatory' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:509:1: ( 'mandatory' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:509:1: ( 'mandatory' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:510:1: 'mandatory'
            {
             before(grammarAccess.getMandatoryAccess().getMandatoryKeyword_0()); 
            match(input,14,FOLLOW_14_in_rule__Mandatory__Group__0__Impl991); 
             after(grammarAccess.getMandatoryAccess().getMandatoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__0__Impl"


    // $ANTLR start "rule__Mandatory__Group__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:523:1: rule__Mandatory__Group__1 : rule__Mandatory__Group__1__Impl ;
    public final void rule__Mandatory__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:527:1: ( rule__Mandatory__Group__1__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:528:2: rule__Mandatory__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Mandatory__Group__1__Impl_in_rule__Mandatory__Group__11022);
            rule__Mandatory__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__1"


    // $ANTLR start "rule__Mandatory__Group__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:534:1: rule__Mandatory__Group__1__Impl : ( ( rule__Mandatory__VideoSequencesAssignment_1 ) ) ;
    public final void rule__Mandatory__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:538:1: ( ( ( rule__Mandatory__VideoSequencesAssignment_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:539:1: ( ( rule__Mandatory__VideoSequencesAssignment_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:539:1: ( ( rule__Mandatory__VideoSequencesAssignment_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:540:1: ( rule__Mandatory__VideoSequencesAssignment_1 )
            {
             before(grammarAccess.getMandatoryAccess().getVideoSequencesAssignment_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:541:1: ( rule__Mandatory__VideoSequencesAssignment_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:541:2: rule__Mandatory__VideoSequencesAssignment_1
            {
            pushFollow(FOLLOW_rule__Mandatory__VideoSequencesAssignment_1_in_rule__Mandatory__Group__1__Impl1049);
            rule__Mandatory__VideoSequencesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMandatoryAccess().getVideoSequencesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__1__Impl"


    // $ANTLR start "rule__Optional__Group__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:555:1: rule__Optional__Group__0 : rule__Optional__Group__0__Impl rule__Optional__Group__1 ;
    public final void rule__Optional__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:559:1: ( rule__Optional__Group__0__Impl rule__Optional__Group__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:560:2: rule__Optional__Group__0__Impl rule__Optional__Group__1
            {
            pushFollow(FOLLOW_rule__Optional__Group__0__Impl_in_rule__Optional__Group__01083);
            rule__Optional__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Optional__Group__1_in_rule__Optional__Group__01086);
            rule__Optional__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__0"


    // $ANTLR start "rule__Optional__Group__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:567:1: rule__Optional__Group__0__Impl : ( 'optional' ) ;
    public final void rule__Optional__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:571:1: ( ( 'optional' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:572:1: ( 'optional' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:572:1: ( 'optional' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:573:1: 'optional'
            {
             before(grammarAccess.getOptionalAccess().getOptionalKeyword_0()); 
            match(input,15,FOLLOW_15_in_rule__Optional__Group__0__Impl1114); 
             after(grammarAccess.getOptionalAccess().getOptionalKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__0__Impl"


    // $ANTLR start "rule__Optional__Group__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:586:1: rule__Optional__Group__1 : rule__Optional__Group__1__Impl ;
    public final void rule__Optional__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:590:1: ( rule__Optional__Group__1__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:591:2: rule__Optional__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Optional__Group__1__Impl_in_rule__Optional__Group__11145);
            rule__Optional__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__1"


    // $ANTLR start "rule__Optional__Group__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:597:1: rule__Optional__Group__1__Impl : ( ( rule__Optional__VideoSequencesAssignment_1 ) ) ;
    public final void rule__Optional__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:601:1: ( ( ( rule__Optional__VideoSequencesAssignment_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:602:1: ( ( rule__Optional__VideoSequencesAssignment_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:602:1: ( ( rule__Optional__VideoSequencesAssignment_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:603:1: ( rule__Optional__VideoSequencesAssignment_1 )
            {
             before(grammarAccess.getOptionalAccess().getVideoSequencesAssignment_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:604:1: ( rule__Optional__VideoSequencesAssignment_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:604:2: rule__Optional__VideoSequencesAssignment_1
            {
            pushFollow(FOLLOW_rule__Optional__VideoSequencesAssignment_1_in_rule__Optional__Group__1__Impl1172);
            rule__Optional__VideoSequencesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionalAccess().getVideoSequencesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__1__Impl"


    // $ANTLR start "rule__VideoSequence__Group__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:618:1: rule__VideoSequence__Group__0 : rule__VideoSequence__Group__0__Impl rule__VideoSequence__Group__1 ;
    public final void rule__VideoSequence__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:622:1: ( rule__VideoSequence__Group__0__Impl rule__VideoSequence__Group__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:623:2: rule__VideoSequence__Group__0__Impl rule__VideoSequence__Group__1
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__0__Impl_in_rule__VideoSequence__Group__01206);
            rule__VideoSequence__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group__1_in_rule__VideoSequence__Group__01209);
            rule__VideoSequence__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__0"


    // $ANTLR start "rule__VideoSequence__Group__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:630:1: rule__VideoSequence__Group__0__Impl : ( 'videoseq' ) ;
    public final void rule__VideoSequence__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:634:1: ( ( 'videoseq' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:635:1: ( 'videoseq' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:635:1: ( 'videoseq' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:636:1: 'videoseq'
            {
             before(grammarAccess.getVideoSequenceAccess().getVideoseqKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__VideoSequence__Group__0__Impl1237); 
             after(grammarAccess.getVideoSequenceAccess().getVideoseqKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__0__Impl"


    // $ANTLR start "rule__VideoSequence__Group__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:649:1: rule__VideoSequence__Group__1 : rule__VideoSequence__Group__1__Impl rule__VideoSequence__Group__2 ;
    public final void rule__VideoSequence__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:653:1: ( rule__VideoSequence__Group__1__Impl rule__VideoSequence__Group__2 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:654:2: rule__VideoSequence__Group__1__Impl rule__VideoSequence__Group__2
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__1__Impl_in_rule__VideoSequence__Group__11268);
            rule__VideoSequence__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group__2_in_rule__VideoSequence__Group__11271);
            rule__VideoSequence__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__1"


    // $ANTLR start "rule__VideoSequence__Group__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:661:1: rule__VideoSequence__Group__1__Impl : ( ( rule__VideoSequence__IdAssignment_1 ) ) ;
    public final void rule__VideoSequence__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:665:1: ( ( ( rule__VideoSequence__IdAssignment_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:666:1: ( ( rule__VideoSequence__IdAssignment_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:666:1: ( ( rule__VideoSequence__IdAssignment_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:667:1: ( rule__VideoSequence__IdAssignment_1 )
            {
             before(grammarAccess.getVideoSequenceAccess().getIdAssignment_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:668:1: ( rule__VideoSequence__IdAssignment_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:668:2: rule__VideoSequence__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__VideoSequence__IdAssignment_1_in_rule__VideoSequence__Group__1__Impl1298);
            rule__VideoSequence__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__1__Impl"


    // $ANTLR start "rule__VideoSequence__Group__2"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:678:1: rule__VideoSequence__Group__2 : rule__VideoSequence__Group__2__Impl rule__VideoSequence__Group__3 ;
    public final void rule__VideoSequence__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:682:1: ( rule__VideoSequence__Group__2__Impl rule__VideoSequence__Group__3 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:683:2: rule__VideoSequence__Group__2__Impl rule__VideoSequence__Group__3
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__2__Impl_in_rule__VideoSequence__Group__21328);
            rule__VideoSequence__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group__3_in_rule__VideoSequence__Group__21331);
            rule__VideoSequence__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__2"


    // $ANTLR start "rule__VideoSequence__Group__2__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:690:1: rule__VideoSequence__Group__2__Impl : ( ( rule__VideoSequence__Group_2__0 )? ) ;
    public final void rule__VideoSequence__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:694:1: ( ( ( rule__VideoSequence__Group_2__0 )? ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:695:1: ( ( rule__VideoSequence__Group_2__0 )? )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:695:1: ( ( rule__VideoSequence__Group_2__0 )? )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:696:1: ( rule__VideoSequence__Group_2__0 )?
            {
             before(grammarAccess.getVideoSequenceAccess().getGroup_2()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:697:1: ( rule__VideoSequence__Group_2__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:697:2: rule__VideoSequence__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__VideoSequence__Group_2__0_in_rule__VideoSequence__Group__2__Impl1358);
                    rule__VideoSequence__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSequenceAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__2__Impl"


    // $ANTLR start "rule__VideoSequence__Group__3"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:707:1: rule__VideoSequence__Group__3 : rule__VideoSequence__Group__3__Impl rule__VideoSequence__Group__4 ;
    public final void rule__VideoSequence__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:711:1: ( rule__VideoSequence__Group__3__Impl rule__VideoSequence__Group__4 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:712:2: rule__VideoSequence__Group__3__Impl rule__VideoSequence__Group__4
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__3__Impl_in_rule__VideoSequence__Group__31389);
            rule__VideoSequence__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group__4_in_rule__VideoSequence__Group__31392);
            rule__VideoSequence__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__3"


    // $ANTLR start "rule__VideoSequence__Group__3__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:719:1: rule__VideoSequence__Group__3__Impl : ( ( rule__VideoSequence__Group_3__0 )? ) ;
    public final void rule__VideoSequence__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:723:1: ( ( ( rule__VideoSequence__Group_3__0 )? ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:724:1: ( ( rule__VideoSequence__Group_3__0 )? )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:724:1: ( ( rule__VideoSequence__Group_3__0 )? )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:725:1: ( rule__VideoSequence__Group_3__0 )?
            {
             before(grammarAccess.getVideoSequenceAccess().getGroup_3()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:726:1: ( rule__VideoSequence__Group_3__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:726:2: rule__VideoSequence__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__VideoSequence__Group_3__0_in_rule__VideoSequence__Group__3__Impl1419);
                    rule__VideoSequence__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSequenceAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__3__Impl"


    // $ANTLR start "rule__VideoSequence__Group__4"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:736:1: rule__VideoSequence__Group__4 : rule__VideoSequence__Group__4__Impl rule__VideoSequence__Group__5 ;
    public final void rule__VideoSequence__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:740:1: ( rule__VideoSequence__Group__4__Impl rule__VideoSequence__Group__5 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:741:2: rule__VideoSequence__Group__4__Impl rule__VideoSequence__Group__5
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__4__Impl_in_rule__VideoSequence__Group__41450);
            rule__VideoSequence__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group__5_in_rule__VideoSequence__Group__41453);
            rule__VideoSequence__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__4"


    // $ANTLR start "rule__VideoSequence__Group__4__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:748:1: rule__VideoSequence__Group__4__Impl : ( ( rule__VideoSequence__Group_4__0 )? ) ;
    public final void rule__VideoSequence__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:752:1: ( ( ( rule__VideoSequence__Group_4__0 )? ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:753:1: ( ( rule__VideoSequence__Group_4__0 )? )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:753:1: ( ( rule__VideoSequence__Group_4__0 )? )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:754:1: ( rule__VideoSequence__Group_4__0 )?
            {
             before(grammarAccess.getVideoSequenceAccess().getGroup_4()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:755:1: ( rule__VideoSequence__Group_4__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:755:2: rule__VideoSequence__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__VideoSequence__Group_4__0_in_rule__VideoSequence__Group__4__Impl1480);
                    rule__VideoSequence__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSequenceAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__4__Impl"


    // $ANTLR start "rule__VideoSequence__Group__5"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:765:1: rule__VideoSequence__Group__5 : rule__VideoSequence__Group__5__Impl ;
    public final void rule__VideoSequence__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:769:1: ( rule__VideoSequence__Group__5__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:770:2: rule__VideoSequence__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group__5__Impl_in_rule__VideoSequence__Group__51511);
            rule__VideoSequence__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__5"


    // $ANTLR start "rule__VideoSequence__Group__5__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:776:1: rule__VideoSequence__Group__5__Impl : ( ( rule__VideoSequence__Group_5__0 ) ) ;
    public final void rule__VideoSequence__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:780:1: ( ( ( rule__VideoSequence__Group_5__0 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:781:1: ( ( rule__VideoSequence__Group_5__0 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:781:1: ( ( rule__VideoSequence__Group_5__0 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:782:1: ( rule__VideoSequence__Group_5__0 )
            {
             before(grammarAccess.getVideoSequenceAccess().getGroup_5()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:783:1: ( rule__VideoSequence__Group_5__0 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:783:2: rule__VideoSequence__Group_5__0
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_5__0_in_rule__VideoSequence__Group__5__Impl1538);
            rule__VideoSequence__Group_5__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group__5__Impl"


    // $ANTLR start "rule__VideoSequence__Group_2__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:805:1: rule__VideoSequence__Group_2__0 : rule__VideoSequence__Group_2__0__Impl rule__VideoSequence__Group_2__1 ;
    public final void rule__VideoSequence__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:809:1: ( rule__VideoSequence__Group_2__0__Impl rule__VideoSequence__Group_2__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:810:2: rule__VideoSequence__Group_2__0__Impl rule__VideoSequence__Group_2__1
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_2__0__Impl_in_rule__VideoSequence__Group_2__01580);
            rule__VideoSequence__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group_2__1_in_rule__VideoSequence__Group_2__01583);
            rule__VideoSequence__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_2__0"


    // $ANTLR start "rule__VideoSequence__Group_2__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:817:1: rule__VideoSequence__Group_2__0__Impl : ( 'duration=' ) ;
    public final void rule__VideoSequence__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:821:1: ( ( 'duration=' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:822:1: ( 'duration=' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:822:1: ( 'duration=' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:823:1: 'duration='
            {
             before(grammarAccess.getVideoSequenceAccess().getDurationKeyword_2_0()); 
            match(input,17,FOLLOW_17_in_rule__VideoSequence__Group_2__0__Impl1611); 
             after(grammarAccess.getVideoSequenceAccess().getDurationKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_2__0__Impl"


    // $ANTLR start "rule__VideoSequence__Group_2__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:836:1: rule__VideoSequence__Group_2__1 : rule__VideoSequence__Group_2__1__Impl ;
    public final void rule__VideoSequence__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:840:1: ( rule__VideoSequence__Group_2__1__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:841:2: rule__VideoSequence__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_2__1__Impl_in_rule__VideoSequence__Group_2__11642);
            rule__VideoSequence__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_2__1"


    // $ANTLR start "rule__VideoSequence__Group_2__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:847:1: rule__VideoSequence__Group_2__1__Impl : ( ( rule__VideoSequence__DurationAssignment_2_1 ) ) ;
    public final void rule__VideoSequence__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:851:1: ( ( ( rule__VideoSequence__DurationAssignment_2_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:852:1: ( ( rule__VideoSequence__DurationAssignment_2_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:852:1: ( ( rule__VideoSequence__DurationAssignment_2_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:853:1: ( rule__VideoSequence__DurationAssignment_2_1 )
            {
             before(grammarAccess.getVideoSequenceAccess().getDurationAssignment_2_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:854:1: ( rule__VideoSequence__DurationAssignment_2_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:854:2: rule__VideoSequence__DurationAssignment_2_1
            {
            pushFollow(FOLLOW_rule__VideoSequence__DurationAssignment_2_1_in_rule__VideoSequence__Group_2__1__Impl1669);
            rule__VideoSequence__DurationAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getDurationAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_2__1__Impl"


    // $ANTLR start "rule__VideoSequence__Group_3__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:868:1: rule__VideoSequence__Group_3__0 : rule__VideoSequence__Group_3__0__Impl rule__VideoSequence__Group_3__1 ;
    public final void rule__VideoSequence__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:872:1: ( rule__VideoSequence__Group_3__0__Impl rule__VideoSequence__Group_3__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:873:2: rule__VideoSequence__Group_3__0__Impl rule__VideoSequence__Group_3__1
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_3__0__Impl_in_rule__VideoSequence__Group_3__01703);
            rule__VideoSequence__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group_3__1_in_rule__VideoSequence__Group_3__01706);
            rule__VideoSequence__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_3__0"


    // $ANTLR start "rule__VideoSequence__Group_3__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:880:1: rule__VideoSequence__Group_3__0__Impl : ( 'nomVideo=' ) ;
    public final void rule__VideoSequence__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:884:1: ( ( 'nomVideo=' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:885:1: ( 'nomVideo=' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:885:1: ( 'nomVideo=' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:886:1: 'nomVideo='
            {
             before(grammarAccess.getVideoSequenceAccess().getNomVideoKeyword_3_0()); 
            match(input,18,FOLLOW_18_in_rule__VideoSequence__Group_3__0__Impl1734); 
             after(grammarAccess.getVideoSequenceAccess().getNomVideoKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_3__0__Impl"


    // $ANTLR start "rule__VideoSequence__Group_3__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:899:1: rule__VideoSequence__Group_3__1 : rule__VideoSequence__Group_3__1__Impl ;
    public final void rule__VideoSequence__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:903:1: ( rule__VideoSequence__Group_3__1__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:904:2: rule__VideoSequence__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_3__1__Impl_in_rule__VideoSequence__Group_3__11765);
            rule__VideoSequence__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_3__1"


    // $ANTLR start "rule__VideoSequence__Group_3__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:910:1: rule__VideoSequence__Group_3__1__Impl : ( ( rule__VideoSequence__NomVideoAssignment_3_1 ) ) ;
    public final void rule__VideoSequence__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:914:1: ( ( ( rule__VideoSequence__NomVideoAssignment_3_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:915:1: ( ( rule__VideoSequence__NomVideoAssignment_3_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:915:1: ( ( rule__VideoSequence__NomVideoAssignment_3_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:916:1: ( rule__VideoSequence__NomVideoAssignment_3_1 )
            {
             before(grammarAccess.getVideoSequenceAccess().getNomVideoAssignment_3_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:917:1: ( rule__VideoSequence__NomVideoAssignment_3_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:917:2: rule__VideoSequence__NomVideoAssignment_3_1
            {
            pushFollow(FOLLOW_rule__VideoSequence__NomVideoAssignment_3_1_in_rule__VideoSequence__Group_3__1__Impl1792);
            rule__VideoSequence__NomVideoAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getNomVideoAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_3__1__Impl"


    // $ANTLR start "rule__VideoSequence__Group_4__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:931:1: rule__VideoSequence__Group_4__0 : rule__VideoSequence__Group_4__0__Impl rule__VideoSequence__Group_4__1 ;
    public final void rule__VideoSequence__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:935:1: ( rule__VideoSequence__Group_4__0__Impl rule__VideoSequence__Group_4__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:936:2: rule__VideoSequence__Group_4__0__Impl rule__VideoSequence__Group_4__1
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_4__0__Impl_in_rule__VideoSequence__Group_4__01826);
            rule__VideoSequence__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group_4__1_in_rule__VideoSequence__Group_4__01829);
            rule__VideoSequence__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_4__0"


    // $ANTLR start "rule__VideoSequence__Group_4__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:943:1: rule__VideoSequence__Group_4__0__Impl : ( 'description=' ) ;
    public final void rule__VideoSequence__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:947:1: ( ( 'description=' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:948:1: ( 'description=' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:948:1: ( 'description=' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:949:1: 'description='
            {
             before(grammarAccess.getVideoSequenceAccess().getDescriptionKeyword_4_0()); 
            match(input,19,FOLLOW_19_in_rule__VideoSequence__Group_4__0__Impl1857); 
             after(grammarAccess.getVideoSequenceAccess().getDescriptionKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_4__0__Impl"


    // $ANTLR start "rule__VideoSequence__Group_4__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:962:1: rule__VideoSequence__Group_4__1 : rule__VideoSequence__Group_4__1__Impl ;
    public final void rule__VideoSequence__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:966:1: ( rule__VideoSequence__Group_4__1__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:967:2: rule__VideoSequence__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_4__1__Impl_in_rule__VideoSequence__Group_4__11888);
            rule__VideoSequence__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_4__1"


    // $ANTLR start "rule__VideoSequence__Group_4__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:973:1: rule__VideoSequence__Group_4__1__Impl : ( ( rule__VideoSequence__DescriptionAssignment_4_1 ) ) ;
    public final void rule__VideoSequence__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:977:1: ( ( ( rule__VideoSequence__DescriptionAssignment_4_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:978:1: ( ( rule__VideoSequence__DescriptionAssignment_4_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:978:1: ( ( rule__VideoSequence__DescriptionAssignment_4_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:979:1: ( rule__VideoSequence__DescriptionAssignment_4_1 )
            {
             before(grammarAccess.getVideoSequenceAccess().getDescriptionAssignment_4_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:980:1: ( rule__VideoSequence__DescriptionAssignment_4_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:980:2: rule__VideoSequence__DescriptionAssignment_4_1
            {
            pushFollow(FOLLOW_rule__VideoSequence__DescriptionAssignment_4_1_in_rule__VideoSequence__Group_4__1__Impl1915);
            rule__VideoSequence__DescriptionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getDescriptionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_4__1__Impl"


    // $ANTLR start "rule__VideoSequence__Group_5__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:994:1: rule__VideoSequence__Group_5__0 : rule__VideoSequence__Group_5__0__Impl rule__VideoSequence__Group_5__1 ;
    public final void rule__VideoSequence__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:998:1: ( rule__VideoSequence__Group_5__0__Impl rule__VideoSequence__Group_5__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:999:2: rule__VideoSequence__Group_5__0__Impl rule__VideoSequence__Group_5__1
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_5__0__Impl_in_rule__VideoSequence__Group_5__01949);
            rule__VideoSequence__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSequence__Group_5__1_in_rule__VideoSequence__Group_5__01952);
            rule__VideoSequence__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_5__0"


    // $ANTLR start "rule__VideoSequence__Group_5__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1006:1: rule__VideoSequence__Group_5__0__Impl : ( 'url=' ) ;
    public final void rule__VideoSequence__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1010:1: ( ( 'url=' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1011:1: ( 'url=' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1011:1: ( 'url=' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1012:1: 'url='
            {
             before(grammarAccess.getVideoSequenceAccess().getUrlKeyword_5_0()); 
            match(input,20,FOLLOW_20_in_rule__VideoSequence__Group_5__0__Impl1980); 
             after(grammarAccess.getVideoSequenceAccess().getUrlKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_5__0__Impl"


    // $ANTLR start "rule__VideoSequence__Group_5__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1025:1: rule__VideoSequence__Group_5__1 : rule__VideoSequence__Group_5__1__Impl ;
    public final void rule__VideoSequence__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1029:1: ( rule__VideoSequence__Group_5__1__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1030:2: rule__VideoSequence__Group_5__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSequence__Group_5__1__Impl_in_rule__VideoSequence__Group_5__12011);
            rule__VideoSequence__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_5__1"


    // $ANTLR start "rule__VideoSequence__Group_5__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1036:1: rule__VideoSequence__Group_5__1__Impl : ( ( rule__VideoSequence__UrlAssignment_5_1 ) ) ;
    public final void rule__VideoSequence__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1040:1: ( ( ( rule__VideoSequence__UrlAssignment_5_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1041:1: ( ( rule__VideoSequence__UrlAssignment_5_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1041:1: ( ( rule__VideoSequence__UrlAssignment_5_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1042:1: ( rule__VideoSequence__UrlAssignment_5_1 )
            {
             before(grammarAccess.getVideoSequenceAccess().getUrlAssignment_5_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1043:1: ( rule__VideoSequence__UrlAssignment_5_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1043:2: rule__VideoSequence__UrlAssignment_5_1
            {
            pushFollow(FOLLOW_rule__VideoSequence__UrlAssignment_5_1_in_rule__VideoSequence__Group_5__1__Impl2038);
            rule__VideoSequence__UrlAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSequenceAccess().getUrlAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__Group_5__1__Impl"


    // $ANTLR start "rule__Alternatives__Group__0"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1057:1: rule__Alternatives__Group__0 : rule__Alternatives__Group__0__Impl rule__Alternatives__Group__1 ;
    public final void rule__Alternatives__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1061:1: ( rule__Alternatives__Group__0__Impl rule__Alternatives__Group__1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1062:2: rule__Alternatives__Group__0__Impl rule__Alternatives__Group__1
            {
            pushFollow(FOLLOW_rule__Alternatives__Group__0__Impl_in_rule__Alternatives__Group__02072);
            rule__Alternatives__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternatives__Group__1_in_rule__Alternatives__Group__02075);
            rule__Alternatives__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__0"


    // $ANTLR start "rule__Alternatives__Group__0__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1069:1: rule__Alternatives__Group__0__Impl : ( 'alternative' ) ;
    public final void rule__Alternatives__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1073:1: ( ( 'alternative' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1074:1: ( 'alternative' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1074:1: ( 'alternative' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1075:1: 'alternative'
            {
             before(grammarAccess.getAlternativesAccess().getAlternativeKeyword_0()); 
            match(input,21,FOLLOW_21_in_rule__Alternatives__Group__0__Impl2103); 
             after(grammarAccess.getAlternativesAccess().getAlternativeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__0__Impl"


    // $ANTLR start "rule__Alternatives__Group__1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1088:1: rule__Alternatives__Group__1 : rule__Alternatives__Group__1__Impl rule__Alternatives__Group__2 ;
    public final void rule__Alternatives__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1092:1: ( rule__Alternatives__Group__1__Impl rule__Alternatives__Group__2 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1093:2: rule__Alternatives__Group__1__Impl rule__Alternatives__Group__2
            {
            pushFollow(FOLLOW_rule__Alternatives__Group__1__Impl_in_rule__Alternatives__Group__12134);
            rule__Alternatives__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternatives__Group__2_in_rule__Alternatives__Group__12137);
            rule__Alternatives__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__1"


    // $ANTLR start "rule__Alternatives__Group__1__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1100:1: rule__Alternatives__Group__1__Impl : ( ( rule__Alternatives__IdAssignment_1 ) ) ;
    public final void rule__Alternatives__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1104:1: ( ( ( rule__Alternatives__IdAssignment_1 ) ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1105:1: ( ( rule__Alternatives__IdAssignment_1 ) )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1105:1: ( ( rule__Alternatives__IdAssignment_1 ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1106:1: ( rule__Alternatives__IdAssignment_1 )
            {
             before(grammarAccess.getAlternativesAccess().getIdAssignment_1()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1107:1: ( rule__Alternatives__IdAssignment_1 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1107:2: rule__Alternatives__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__Alternatives__IdAssignment_1_in_rule__Alternatives__Group__1__Impl2164);
            rule__Alternatives__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAlternativesAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__1__Impl"


    // $ANTLR start "rule__Alternatives__Group__2"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1117:1: rule__Alternatives__Group__2 : rule__Alternatives__Group__2__Impl rule__Alternatives__Group__3 ;
    public final void rule__Alternatives__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1121:1: ( rule__Alternatives__Group__2__Impl rule__Alternatives__Group__3 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1122:2: rule__Alternatives__Group__2__Impl rule__Alternatives__Group__3
            {
            pushFollow(FOLLOW_rule__Alternatives__Group__2__Impl_in_rule__Alternatives__Group__22194);
            rule__Alternatives__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternatives__Group__3_in_rule__Alternatives__Group__22197);
            rule__Alternatives__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__2"


    // $ANTLR start "rule__Alternatives__Group__2__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1129:1: rule__Alternatives__Group__2__Impl : ( '{' ) ;
    public final void rule__Alternatives__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1133:1: ( ( '{' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1134:1: ( '{' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1134:1: ( '{' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1135:1: '{'
            {
             before(grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Alternatives__Group__2__Impl2225); 
             after(grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__2__Impl"


    // $ANTLR start "rule__Alternatives__Group__3"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1148:1: rule__Alternatives__Group__3 : rule__Alternatives__Group__3__Impl rule__Alternatives__Group__4 ;
    public final void rule__Alternatives__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1152:1: ( rule__Alternatives__Group__3__Impl rule__Alternatives__Group__4 )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1153:2: rule__Alternatives__Group__3__Impl rule__Alternatives__Group__4
            {
            pushFollow(FOLLOW_rule__Alternatives__Group__3__Impl_in_rule__Alternatives__Group__32256);
            rule__Alternatives__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternatives__Group__4_in_rule__Alternatives__Group__32259);
            rule__Alternatives__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__3"


    // $ANTLR start "rule__Alternatives__Group__3__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1160:1: rule__Alternatives__Group__3__Impl : ( ( rule__Alternatives__VideoSequencesAssignment_3 )* ) ;
    public final void rule__Alternatives__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1164:1: ( ( ( rule__Alternatives__VideoSequencesAssignment_3 )* ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1165:1: ( ( rule__Alternatives__VideoSequencesAssignment_3 )* )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1165:1: ( ( rule__Alternatives__VideoSequencesAssignment_3 )* )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1166:1: ( rule__Alternatives__VideoSequencesAssignment_3 )*
            {
             before(grammarAccess.getAlternativesAccess().getVideoSequencesAssignment_3()); 
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1167:1: ( rule__Alternatives__VideoSequencesAssignment_3 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1167:2: rule__Alternatives__VideoSequencesAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Alternatives__VideoSequencesAssignment_3_in_rule__Alternatives__Group__3__Impl2286);
            	    rule__Alternatives__VideoSequencesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getAlternativesAccess().getVideoSequencesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__3__Impl"


    // $ANTLR start "rule__Alternatives__Group__4"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1177:1: rule__Alternatives__Group__4 : rule__Alternatives__Group__4__Impl ;
    public final void rule__Alternatives__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1181:1: ( rule__Alternatives__Group__4__Impl )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1182:2: rule__Alternatives__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Alternatives__Group__4__Impl_in_rule__Alternatives__Group__42317);
            rule__Alternatives__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__4"


    // $ANTLR start "rule__Alternatives__Group__4__Impl"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1188:1: rule__Alternatives__Group__4__Impl : ( '}' ) ;
    public final void rule__Alternatives__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1192:1: ( ( '}' ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1193:1: ( '}' )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1193:1: ( '}' )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1194:1: '}'
            {
             before(grammarAccess.getAlternativesAccess().getRightCurlyBracketKeyword_4()); 
            match(input,13,FOLLOW_13_in_rule__Alternatives__Group__4__Impl2345); 
             after(grammarAccess.getAlternativesAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__Group__4__Impl"


    // $ANTLR start "rule__VideoGen__ListAssignment_3"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1218:1: rule__VideoGen__ListAssignment_3 : ( ruleSequence ) ;
    public final void rule__VideoGen__ListAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1222:1: ( ( ruleSequence ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1223:1: ( ruleSequence )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1223:1: ( ruleSequence )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1224:1: ruleSequence
            {
             before(grammarAccess.getVideoGenAccess().getListSequenceParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleSequence_in_rule__VideoGen__ListAssignment_32391);
            ruleSequence();

            state._fsp--;

             after(grammarAccess.getVideoGenAccess().getListSequenceParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__ListAssignment_3"


    // $ANTLR start "rule__Mandatory__VideoSequencesAssignment_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1233:1: rule__Mandatory__VideoSequencesAssignment_1 : ( ruleVideoSequence ) ;
    public final void rule__Mandatory__VideoSequencesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1237:1: ( ( ruleVideoSequence ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1238:1: ( ruleVideoSequence )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1238:1: ( ruleVideoSequence )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1239:1: ruleVideoSequence
            {
             before(grammarAccess.getMandatoryAccess().getVideoSequencesVideoSequenceParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVideoSequence_in_rule__Mandatory__VideoSequencesAssignment_12422);
            ruleVideoSequence();

            state._fsp--;

             after(grammarAccess.getMandatoryAccess().getVideoSequencesVideoSequenceParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__VideoSequencesAssignment_1"


    // $ANTLR start "rule__Optional__VideoSequencesAssignment_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1248:1: rule__Optional__VideoSequencesAssignment_1 : ( ruleVideoSequence ) ;
    public final void rule__Optional__VideoSequencesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1252:1: ( ( ruleVideoSequence ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1253:1: ( ruleVideoSequence )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1253:1: ( ruleVideoSequence )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1254:1: ruleVideoSequence
            {
             before(grammarAccess.getOptionalAccess().getVideoSequencesVideoSequenceParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVideoSequence_in_rule__Optional__VideoSequencesAssignment_12453);
            ruleVideoSequence();

            state._fsp--;

             after(grammarAccess.getOptionalAccess().getVideoSequencesVideoSequenceParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__VideoSequencesAssignment_1"


    // $ANTLR start "rule__VideoSequence__IdAssignment_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1263:1: rule__VideoSequence__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__VideoSequence__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1267:1: ( ( RULE_ID ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1268:1: ( RULE_ID )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1268:1: ( RULE_ID )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1269:1: RULE_ID
            {
             before(grammarAccess.getVideoSequenceAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__VideoSequence__IdAssignment_12484); 
             after(grammarAccess.getVideoSequenceAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__IdAssignment_1"


    // $ANTLR start "rule__VideoSequence__DurationAssignment_2_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1278:1: rule__VideoSequence__DurationAssignment_2_1 : ( RULE_STRING ) ;
    public final void rule__VideoSequence__DurationAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1282:1: ( ( RULE_STRING ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1283:1: ( RULE_STRING )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1283:1: ( RULE_STRING )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1284:1: RULE_STRING
            {
             before(grammarAccess.getVideoSequenceAccess().getDurationSTRINGTerminalRuleCall_2_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VideoSequence__DurationAssignment_2_12515); 
             after(grammarAccess.getVideoSequenceAccess().getDurationSTRINGTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__DurationAssignment_2_1"


    // $ANTLR start "rule__VideoSequence__NomVideoAssignment_3_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1293:1: rule__VideoSequence__NomVideoAssignment_3_1 : ( RULE_STRING ) ;
    public final void rule__VideoSequence__NomVideoAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1297:1: ( ( RULE_STRING ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1298:1: ( RULE_STRING )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1298:1: ( RULE_STRING )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1299:1: RULE_STRING
            {
             before(grammarAccess.getVideoSequenceAccess().getNomVideoSTRINGTerminalRuleCall_3_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VideoSequence__NomVideoAssignment_3_12546); 
             after(grammarAccess.getVideoSequenceAccess().getNomVideoSTRINGTerminalRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__NomVideoAssignment_3_1"


    // $ANTLR start "rule__VideoSequence__DescriptionAssignment_4_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1308:1: rule__VideoSequence__DescriptionAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__VideoSequence__DescriptionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1312:1: ( ( RULE_STRING ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1313:1: ( RULE_STRING )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1313:1: ( RULE_STRING )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1314:1: RULE_STRING
            {
             before(grammarAccess.getVideoSequenceAccess().getDescriptionSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VideoSequence__DescriptionAssignment_4_12577); 
             after(grammarAccess.getVideoSequenceAccess().getDescriptionSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__DescriptionAssignment_4_1"


    // $ANTLR start "rule__VideoSequence__UrlAssignment_5_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1323:1: rule__VideoSequence__UrlAssignment_5_1 : ( RULE_STRING ) ;
    public final void rule__VideoSequence__UrlAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1327:1: ( ( RULE_STRING ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1328:1: ( RULE_STRING )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1328:1: ( RULE_STRING )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1329:1: RULE_STRING
            {
             before(grammarAccess.getVideoSequenceAccess().getUrlSTRINGTerminalRuleCall_5_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VideoSequence__UrlAssignment_5_12608); 
             after(grammarAccess.getVideoSequenceAccess().getUrlSTRINGTerminalRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSequence__UrlAssignment_5_1"


    // $ANTLR start "rule__Alternatives__IdAssignment_1"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1338:1: rule__Alternatives__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Alternatives__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1342:1: ( ( RULE_ID ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1343:1: ( RULE_ID )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1343:1: ( RULE_ID )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1344:1: RULE_ID
            {
             before(grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Alternatives__IdAssignment_12639); 
             after(grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__IdAssignment_1"


    // $ANTLR start "rule__Alternatives__VideoSequencesAssignment_3"
    // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1353:1: rule__Alternatives__VideoSequencesAssignment_3 : ( ruleVideoSequence ) ;
    public final void rule__Alternatives__VideoSequencesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1357:1: ( ( ruleVideoSequence ) )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1358:1: ( ruleVideoSequence )
            {
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1358:1: ( ruleVideoSequence )
            // ../fr.istic.gla.idm.videoGen.ui/src-gen/fr/istic/gla/idm/ui/contentassist/antlr/internal/InternalVideoGen.g:1359:1: ruleVideoSequence
            {
             before(grammarAccess.getAlternativesAccess().getVideoSequencesVideoSequenceParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleVideoSequence_in_rule__Alternatives__VideoSequencesAssignment_32670);
            ruleVideoSequence();

            state._fsp--;

             after(grammarAccess.getAlternativesAccess().getVideoSequencesVideoSequenceParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternatives__VideoSequencesAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleVideoGen_in_entryRuleVideoGen61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoGen68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__0_in_ruleVideoGen94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSequence_in_entryRuleSequence121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSequence128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Sequence__Alternatives_in_ruleSequence154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSingle_in_entryRuleSingle181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSingle188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Single__Alternatives_in_ruleSingle214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiple_in_entryRuleMultiple241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMultiple248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternatives_in_ruleMultiple274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_entryRuleMandatory300 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMandatory307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__0_in_ruleMandatory333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_entryRuleOptional360 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOptional367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__Group__0_in_ruleOptional393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_entryRuleVideoSequence420 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoSequence427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__0_in_ruleVideoSequence453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternatives_in_entryRuleAlternatives480 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAlternatives487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__0_in_ruleAlternatives513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSingle_in_rule__Sequence__Alternatives549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiple_in_rule__Sequence__Alternatives566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_rule__Single__Alternatives598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_rule__Single__Alternatives615 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__0__Impl_in_rule__VideoGen__Group__0645 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__1_in_rule__VideoGen__Group__0648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__1__Impl_in_rule__VideoGen__Group__1706 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__2_in_rule__VideoGen__Group__1709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__VideoGen__Group__1__Impl737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__2__Impl_in_rule__VideoGen__Group__2768 = new BitSet(new long[]{0x000000000020E000L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__3_in_rule__VideoGen__Group__2771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__VideoGen__Group__2__Impl799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__3__Impl_in_rule__VideoGen__Group__3830 = new BitSet(new long[]{0x000000000020E000L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__4_in_rule__VideoGen__Group__3833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__ListAssignment_3_in_rule__VideoGen__Group__3__Impl860 = new BitSet(new long[]{0x000000000020C002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__4__Impl_in_rule__VideoGen__Group__4891 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__VideoGen__Group__4__Impl919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__0__Impl_in_rule__Mandatory__Group__0960 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__1_in_rule__Mandatory__Group__0963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Mandatory__Group__0__Impl991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__1__Impl_in_rule__Mandatory__Group__11022 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__VideoSequencesAssignment_1_in_rule__Mandatory__Group__1__Impl1049 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__Group__0__Impl_in_rule__Optional__Group__01083 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Optional__Group__1_in_rule__Optional__Group__01086 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Optional__Group__0__Impl1114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__Group__1__Impl_in_rule__Optional__Group__11145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__VideoSequencesAssignment_1_in_rule__Optional__Group__1__Impl1172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__0__Impl_in_rule__VideoSequence__Group__01206 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__1_in_rule__VideoSequence__Group__01209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__VideoSequence__Group__0__Impl1237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__1__Impl_in_rule__VideoSequence__Group__11268 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__2_in_rule__VideoSequence__Group__11271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__IdAssignment_1_in_rule__VideoSequence__Group__1__Impl1298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__2__Impl_in_rule__VideoSequence__Group__21328 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__3_in_rule__VideoSequence__Group__21331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_2__0_in_rule__VideoSequence__Group__2__Impl1358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__3__Impl_in_rule__VideoSequence__Group__31389 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__4_in_rule__VideoSequence__Group__31392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_3__0_in_rule__VideoSequence__Group__3__Impl1419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__4__Impl_in_rule__VideoSequence__Group__41450 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__5_in_rule__VideoSequence__Group__41453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_4__0_in_rule__VideoSequence__Group__4__Impl1480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group__5__Impl_in_rule__VideoSequence__Group__51511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_5__0_in_rule__VideoSequence__Group__5__Impl1538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_2__0__Impl_in_rule__VideoSequence__Group_2__01580 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_2__1_in_rule__VideoSequence__Group_2__01583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__VideoSequence__Group_2__0__Impl1611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_2__1__Impl_in_rule__VideoSequence__Group_2__11642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__DurationAssignment_2_1_in_rule__VideoSequence__Group_2__1__Impl1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_3__0__Impl_in_rule__VideoSequence__Group_3__01703 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_3__1_in_rule__VideoSequence__Group_3__01706 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__VideoSequence__Group_3__0__Impl1734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_3__1__Impl_in_rule__VideoSequence__Group_3__11765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__NomVideoAssignment_3_1_in_rule__VideoSequence__Group_3__1__Impl1792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_4__0__Impl_in_rule__VideoSequence__Group_4__01826 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_4__1_in_rule__VideoSequence__Group_4__01829 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__VideoSequence__Group_4__0__Impl1857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_4__1__Impl_in_rule__VideoSequence__Group_4__11888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__DescriptionAssignment_4_1_in_rule__VideoSequence__Group_4__1__Impl1915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_5__0__Impl_in_rule__VideoSequence__Group_5__01949 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_5__1_in_rule__VideoSequence__Group_5__01952 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__VideoSequence__Group_5__0__Impl1980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__Group_5__1__Impl_in_rule__VideoSequence__Group_5__12011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSequence__UrlAssignment_5_1_in_rule__VideoSequence__Group_5__1__Impl2038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__0__Impl_in_rule__Alternatives__Group__02072 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__1_in_rule__Alternatives__Group__02075 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Alternatives__Group__0__Impl2103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__1__Impl_in_rule__Alternatives__Group__12134 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__2_in_rule__Alternatives__Group__12137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__IdAssignment_1_in_rule__Alternatives__Group__1__Impl2164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__2__Impl_in_rule__Alternatives__Group__22194 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__3_in_rule__Alternatives__Group__22197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Alternatives__Group__2__Impl2225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__3__Impl_in_rule__Alternatives__Group__32256 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__4_in_rule__Alternatives__Group__32259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternatives__VideoSequencesAssignment_3_in_rule__Alternatives__Group__3__Impl2286 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Alternatives__Group__4__Impl_in_rule__Alternatives__Group__42317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Alternatives__Group__4__Impl2345 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSequence_in_rule__VideoGen__ListAssignment_32391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_rule__Mandatory__VideoSequencesAssignment_12422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_rule__Optional__VideoSequencesAssignment_12453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__VideoSequence__IdAssignment_12484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VideoSequence__DurationAssignment_2_12515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VideoSequence__NomVideoAssignment_3_12546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VideoSequence__DescriptionAssignment_4_12577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VideoSequence__UrlAssignment_5_12608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Alternatives__IdAssignment_12639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_rule__Alternatives__VideoSequencesAssignment_32670 = new BitSet(new long[]{0x0000000000000002L});

}