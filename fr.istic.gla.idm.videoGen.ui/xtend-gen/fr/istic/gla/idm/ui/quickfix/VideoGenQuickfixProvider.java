/**
 * generated by Xtext
 */
package fr.istic.gla.idm.ui.quickfix;

import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;

/**
 * Custom quickfixes.
 * 
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#quick-fixes
 */
@SuppressWarnings("all")
public class VideoGenQuickfixProvider extends DefaultQuickfixProvider {
}
