package fr.istic.gla.idm.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.istic.gla.idm.services.VideoGenGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalVideoGenParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'VideoGen'", "'{'", "'}'", "'mandatory'", "'optional'", "'videoseq'", "'duration='", "'nomVideo='", "'description='", "'url='", "'alternative'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalVideoGenParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalVideoGenParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalVideoGenParser.tokenNames; }
    public String getGrammarFileName() { return "../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g"; }



     	private VideoGenGrammarAccess grammarAccess;
     	
        public InternalVideoGenParser(TokenStream input, VideoGenGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "VideoGen";	
       	}
       	
       	@Override
       	protected VideoGenGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleVideoGen"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:67:1: entryRuleVideoGen returns [EObject current=null] : iv_ruleVideoGen= ruleVideoGen EOF ;
    public final EObject entryRuleVideoGen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideoGen = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:68:2: (iv_ruleVideoGen= ruleVideoGen EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:69:2: iv_ruleVideoGen= ruleVideoGen EOF
            {
             newCompositeNode(grammarAccess.getVideoGenRule()); 
            pushFollow(FOLLOW_ruleVideoGen_in_entryRuleVideoGen75);
            iv_ruleVideoGen=ruleVideoGen();

            state._fsp--;

             current =iv_ruleVideoGen; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoGen85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideoGen"


    // $ANTLR start "ruleVideoGen"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:76:1: ruleVideoGen returns [EObject current=null] : ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_list_3_0= ruleSequence ) )* otherlv_4= '}' ) ;
    public final EObject ruleVideoGen() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_list_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:79:28: ( ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_list_3_0= ruleSequence ) )* otherlv_4= '}' ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:80:1: ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_list_3_0= ruleSequence ) )* otherlv_4= '}' )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:80:1: ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_list_3_0= ruleSequence ) )* otherlv_4= '}' )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:80:2: () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_list_3_0= ruleSequence ) )* otherlv_4= '}'
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:80:2: ()
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getVideoGenAccess().getVideoGenAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FOLLOW_11_in_ruleVideoGen131); 

                	newLeafNode(otherlv_1, grammarAccess.getVideoGenAccess().getVideoGenKeyword_1());
                
            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleVideoGen143); 

                	newLeafNode(otherlv_2, grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_2());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:94:1: ( (lv_list_3_0= ruleSequence ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=14 && LA1_0<=15)||LA1_0==21) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:95:1: (lv_list_3_0= ruleSequence )
            	    {
            	    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:95:1: (lv_list_3_0= ruleSequence )
            	    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:96:3: lv_list_3_0= ruleSequence
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getVideoGenAccess().getListSequenceParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleSequence_in_ruleVideoGen164);
            	    lv_list_3_0=ruleSequence();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getVideoGenRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"list",
            	            		lv_list_3_0, 
            	            		"Sequence");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleVideoGen177); 

                	newLeafNode(otherlv_4, grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideoGen"


    // $ANTLR start "entryRuleSequence"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:124:1: entryRuleSequence returns [EObject current=null] : iv_ruleSequence= ruleSequence EOF ;
    public final EObject entryRuleSequence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSequence = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:125:2: (iv_ruleSequence= ruleSequence EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:126:2: iv_ruleSequence= ruleSequence EOF
            {
             newCompositeNode(grammarAccess.getSequenceRule()); 
            pushFollow(FOLLOW_ruleSequence_in_entryRuleSequence213);
            iv_ruleSequence=ruleSequence();

            state._fsp--;

             current =iv_ruleSequence; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSequence223); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSequence"


    // $ANTLR start "ruleSequence"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:133:1: ruleSequence returns [EObject current=null] : (this_Single_0= ruleSingle | this_Multiple_1= ruleMultiple ) ;
    public final EObject ruleSequence() throws RecognitionException {
        EObject current = null;

        EObject this_Single_0 = null;

        EObject this_Multiple_1 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:136:28: ( (this_Single_0= ruleSingle | this_Multiple_1= ruleMultiple ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:137:1: (this_Single_0= ruleSingle | this_Multiple_1= ruleMultiple )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:137:1: (this_Single_0= ruleSingle | this_Multiple_1= ruleMultiple )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>=14 && LA2_0<=15)) ) {
                alt2=1;
            }
            else if ( (LA2_0==21) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:138:5: this_Single_0= ruleSingle
                    {
                     
                            newCompositeNode(grammarAccess.getSequenceAccess().getSingleParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleSingle_in_ruleSequence270);
                    this_Single_0=ruleSingle();

                    state._fsp--;

                     
                            current = this_Single_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:148:5: this_Multiple_1= ruleMultiple
                    {
                     
                            newCompositeNode(grammarAccess.getSequenceAccess().getMultipleParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleMultiple_in_ruleSequence297);
                    this_Multiple_1=ruleMultiple();

                    state._fsp--;

                     
                            current = this_Multiple_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSequence"


    // $ANTLR start "entryRuleSingle"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:164:1: entryRuleSingle returns [EObject current=null] : iv_ruleSingle= ruleSingle EOF ;
    public final EObject entryRuleSingle() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingle = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:165:2: (iv_ruleSingle= ruleSingle EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:166:2: iv_ruleSingle= ruleSingle EOF
            {
             newCompositeNode(grammarAccess.getSingleRule()); 
            pushFollow(FOLLOW_ruleSingle_in_entryRuleSingle332);
            iv_ruleSingle=ruleSingle();

            state._fsp--;

             current =iv_ruleSingle; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSingle342); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingle"


    // $ANTLR start "ruleSingle"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:173:1: ruleSingle returns [EObject current=null] : (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional ) ;
    public final EObject ruleSingle() throws RecognitionException {
        EObject current = null;

        EObject this_Mandatory_0 = null;

        EObject this_Optional_1 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:176:28: ( (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:177:1: (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:177:1: (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            else if ( (LA3_0==15) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:178:5: this_Mandatory_0= ruleMandatory
                    {
                     
                            newCompositeNode(grammarAccess.getSingleAccess().getMandatoryParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleMandatory_in_ruleSingle389);
                    this_Mandatory_0=ruleMandatory();

                    state._fsp--;

                     
                            current = this_Mandatory_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:188:5: this_Optional_1= ruleOptional
                    {
                     
                            newCompositeNode(grammarAccess.getSingleAccess().getOptionalParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleOptional_in_ruleSingle416);
                    this_Optional_1=ruleOptional();

                    state._fsp--;

                     
                            current = this_Optional_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingle"


    // $ANTLR start "entryRuleMultiple"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:204:1: entryRuleMultiple returns [EObject current=null] : iv_ruleMultiple= ruleMultiple EOF ;
    public final EObject entryRuleMultiple() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiple = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:205:2: (iv_ruleMultiple= ruleMultiple EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:206:2: iv_ruleMultiple= ruleMultiple EOF
            {
             newCompositeNode(grammarAccess.getMultipleRule()); 
            pushFollow(FOLLOW_ruleMultiple_in_entryRuleMultiple451);
            iv_ruleMultiple=ruleMultiple();

            state._fsp--;

             current =iv_ruleMultiple; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMultiple461); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiple"


    // $ANTLR start "ruleMultiple"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:213:1: ruleMultiple returns [EObject current=null] : this_Alternatives_0= ruleAlternatives ;
    public final EObject ruleMultiple() throws RecognitionException {
        EObject current = null;

        EObject this_Alternatives_0 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:216:28: (this_Alternatives_0= ruleAlternatives )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:218:5: this_Alternatives_0= ruleAlternatives
            {
             
                    newCompositeNode(grammarAccess.getMultipleAccess().getAlternativesParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleAlternatives_in_ruleMultiple507);
            this_Alternatives_0=ruleAlternatives();

            state._fsp--;

             
                    current = this_Alternatives_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiple"


    // $ANTLR start "entryRuleMandatory"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:234:1: entryRuleMandatory returns [EObject current=null] : iv_ruleMandatory= ruleMandatory EOF ;
    public final EObject entryRuleMandatory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMandatory = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:235:2: (iv_ruleMandatory= ruleMandatory EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:236:2: iv_ruleMandatory= ruleMandatory EOF
            {
             newCompositeNode(grammarAccess.getMandatoryRule()); 
            pushFollow(FOLLOW_ruleMandatory_in_entryRuleMandatory541);
            iv_ruleMandatory=ruleMandatory();

            state._fsp--;

             current =iv_ruleMandatory; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMandatory551); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMandatory"


    // $ANTLR start "ruleMandatory"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:243:1: ruleMandatory returns [EObject current=null] : (otherlv_0= 'mandatory' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) ) ;
    public final EObject ruleMandatory() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_videoSequences_1_0 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:246:28: ( (otherlv_0= 'mandatory' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:247:1: (otherlv_0= 'mandatory' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:247:1: (otherlv_0= 'mandatory' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:247:3: otherlv_0= 'mandatory' ( (lv_videoSequences_1_0= ruleVideoSequence ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleMandatory588); 

                	newLeafNode(otherlv_0, grammarAccess.getMandatoryAccess().getMandatoryKeyword_0());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:251:1: ( (lv_videoSequences_1_0= ruleVideoSequence ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:252:1: (lv_videoSequences_1_0= ruleVideoSequence )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:252:1: (lv_videoSequences_1_0= ruleVideoSequence )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:253:3: lv_videoSequences_1_0= ruleVideoSequence
            {
             
            	        newCompositeNode(grammarAccess.getMandatoryAccess().getVideoSequencesVideoSequenceParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVideoSequence_in_ruleMandatory609);
            lv_videoSequences_1_0=ruleVideoSequence();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMandatoryRule());
            	        }
                   		set(
                   			current, 
                   			"videoSequences",
                    		lv_videoSequences_1_0, 
                    		"VideoSequence");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMandatory"


    // $ANTLR start "entryRuleOptional"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:277:1: entryRuleOptional returns [EObject current=null] : iv_ruleOptional= ruleOptional EOF ;
    public final EObject entryRuleOptional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptional = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:278:2: (iv_ruleOptional= ruleOptional EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:279:2: iv_ruleOptional= ruleOptional EOF
            {
             newCompositeNode(grammarAccess.getOptionalRule()); 
            pushFollow(FOLLOW_ruleOptional_in_entryRuleOptional645);
            iv_ruleOptional=ruleOptional();

            state._fsp--;

             current =iv_ruleOptional; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOptional655); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptional"


    // $ANTLR start "ruleOptional"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:286:1: ruleOptional returns [EObject current=null] : (otherlv_0= 'optional' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) ) ;
    public final EObject ruleOptional() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_videoSequences_1_0 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:289:28: ( (otherlv_0= 'optional' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:290:1: (otherlv_0= 'optional' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:290:1: (otherlv_0= 'optional' ( (lv_videoSequences_1_0= ruleVideoSequence ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:290:3: otherlv_0= 'optional' ( (lv_videoSequences_1_0= ruleVideoSequence ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleOptional692); 

                	newLeafNode(otherlv_0, grammarAccess.getOptionalAccess().getOptionalKeyword_0());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:294:1: ( (lv_videoSequences_1_0= ruleVideoSequence ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:295:1: (lv_videoSequences_1_0= ruleVideoSequence )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:295:1: (lv_videoSequences_1_0= ruleVideoSequence )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:296:3: lv_videoSequences_1_0= ruleVideoSequence
            {
             
            	        newCompositeNode(grammarAccess.getOptionalAccess().getVideoSequencesVideoSequenceParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVideoSequence_in_ruleOptional713);
            lv_videoSequences_1_0=ruleVideoSequence();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOptionalRule());
            	        }
                   		set(
                   			current, 
                   			"videoSequences",
                    		lv_videoSequences_1_0, 
                    		"VideoSequence");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptional"


    // $ANTLR start "entryRuleVideoSequence"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:320:1: entryRuleVideoSequence returns [EObject current=null] : iv_ruleVideoSequence= ruleVideoSequence EOF ;
    public final EObject entryRuleVideoSequence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideoSequence = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:321:2: (iv_ruleVideoSequence= ruleVideoSequence EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:322:2: iv_ruleVideoSequence= ruleVideoSequence EOF
            {
             newCompositeNode(grammarAccess.getVideoSequenceRule()); 
            pushFollow(FOLLOW_ruleVideoSequence_in_entryRuleVideoSequence749);
            iv_ruleVideoSequence=ruleVideoSequence();

            state._fsp--;

             current =iv_ruleVideoSequence; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoSequence759); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideoSequence"


    // $ANTLR start "ruleVideoSequence"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:329:1: ruleVideoSequence returns [EObject current=null] : (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) (otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) ) )? (otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) ) )? (otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) ) ) ) ;
    public final EObject ruleVideoSequence() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token lv_duration_3_0=null;
        Token otherlv_4=null;
        Token lv_nomVideo_5_0=null;
        Token otherlv_6=null;
        Token lv_description_7_0=null;
        Token otherlv_8=null;
        Token lv_url_9_0=null;

         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:332:28: ( (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) (otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) ) )? (otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) ) )? (otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) ) ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:333:1: (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) (otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) ) )? (otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) ) )? (otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) ) ) )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:333:1: (otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) (otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) ) )? (otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) ) )? (otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:333:3: otherlv_0= 'videoseq' ( (lv_id_1_0= RULE_ID ) ) (otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) ) )? (otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) ) )? (otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) ) )? (otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleVideoSequence796); 

                	newLeafNode(otherlv_0, grammarAccess.getVideoSequenceAccess().getVideoseqKeyword_0());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:337:1: ( (lv_id_1_0= RULE_ID ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:338:1: (lv_id_1_0= RULE_ID )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:338:1: (lv_id_1_0= RULE_ID )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:339:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVideoSequence813); 

            			newLeafNode(lv_id_1_0, grammarAccess.getVideoSequenceAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVideoSequenceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:355:2: (otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:355:4: otherlv_2= 'duration=' ( (lv_duration_3_0= RULE_STRING ) )
                    {
                    otherlv_2=(Token)match(input,17,FOLLOW_17_in_ruleVideoSequence831); 

                        	newLeafNode(otherlv_2, grammarAccess.getVideoSequenceAccess().getDurationKeyword_2_0());
                        
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:359:1: ( (lv_duration_3_0= RULE_STRING ) )
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:360:1: (lv_duration_3_0= RULE_STRING )
                    {
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:360:1: (lv_duration_3_0= RULE_STRING )
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:361:3: lv_duration_3_0= RULE_STRING
                    {
                    lv_duration_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVideoSequence848); 

                    			newLeafNode(lv_duration_3_0, grammarAccess.getVideoSequenceAccess().getDurationSTRINGTerminalRuleCall_2_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVideoSequenceRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"duration",
                            		lv_duration_3_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:377:4: (otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:377:6: otherlv_4= 'nomVideo=' ( (lv_nomVideo_5_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,18,FOLLOW_18_in_ruleVideoSequence868); 

                        	newLeafNode(otherlv_4, grammarAccess.getVideoSequenceAccess().getNomVideoKeyword_3_0());
                        
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:381:1: ( (lv_nomVideo_5_0= RULE_STRING ) )
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:382:1: (lv_nomVideo_5_0= RULE_STRING )
                    {
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:382:1: (lv_nomVideo_5_0= RULE_STRING )
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:383:3: lv_nomVideo_5_0= RULE_STRING
                    {
                    lv_nomVideo_5_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVideoSequence885); 

                    			newLeafNode(lv_nomVideo_5_0, grammarAccess.getVideoSequenceAccess().getNomVideoSTRINGTerminalRuleCall_3_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVideoSequenceRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"nomVideo",
                            		lv_nomVideo_5_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:399:4: (otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:399:6: otherlv_6= 'description=' ( (lv_description_7_0= RULE_STRING ) )
                    {
                    otherlv_6=(Token)match(input,19,FOLLOW_19_in_ruleVideoSequence905); 

                        	newLeafNode(otherlv_6, grammarAccess.getVideoSequenceAccess().getDescriptionKeyword_4_0());
                        
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:403:1: ( (lv_description_7_0= RULE_STRING ) )
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:404:1: (lv_description_7_0= RULE_STRING )
                    {
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:404:1: (lv_description_7_0= RULE_STRING )
                    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:405:3: lv_description_7_0= RULE_STRING
                    {
                    lv_description_7_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVideoSequence922); 

                    			newLeafNode(lv_description_7_0, grammarAccess.getVideoSequenceAccess().getDescriptionSTRINGTerminalRuleCall_4_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVideoSequenceRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"description",
                            		lv_description_7_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:421:4: (otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:421:6: otherlv_8= 'url=' ( (lv_url_9_0= RULE_STRING ) )
            {
            otherlv_8=(Token)match(input,20,FOLLOW_20_in_ruleVideoSequence942); 

                	newLeafNode(otherlv_8, grammarAccess.getVideoSequenceAccess().getUrlKeyword_5_0());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:425:1: ( (lv_url_9_0= RULE_STRING ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:426:1: (lv_url_9_0= RULE_STRING )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:426:1: (lv_url_9_0= RULE_STRING )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:427:3: lv_url_9_0= RULE_STRING
            {
            lv_url_9_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVideoSequence959); 

            			newLeafNode(lv_url_9_0, grammarAccess.getVideoSequenceAccess().getUrlSTRINGTerminalRuleCall_5_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVideoSequenceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"url",
                    		lv_url_9_0, 
                    		"STRING");
            	    

            }


            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideoSequence"


    // $ANTLR start "entryRuleAlternatives"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:451:1: entryRuleAlternatives returns [EObject current=null] : iv_ruleAlternatives= ruleAlternatives EOF ;
    public final EObject entryRuleAlternatives() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlternatives = null;


        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:452:2: (iv_ruleAlternatives= ruleAlternatives EOF )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:453:2: iv_ruleAlternatives= ruleAlternatives EOF
            {
             newCompositeNode(grammarAccess.getAlternativesRule()); 
            pushFollow(FOLLOW_ruleAlternatives_in_entryRuleAlternatives1001);
            iv_ruleAlternatives=ruleAlternatives();

            state._fsp--;

             current =iv_ruleAlternatives; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAlternatives1011); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlternatives"


    // $ANTLR start "ruleAlternatives"
    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:460:1: ruleAlternatives returns [EObject current=null] : (otherlv_0= 'alternative' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_videoSequences_3_0= ruleVideoSequence ) )* otherlv_4= '}' ) ;
    public final EObject ruleAlternatives() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_videoSequences_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:463:28: ( (otherlv_0= 'alternative' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_videoSequences_3_0= ruleVideoSequence ) )* otherlv_4= '}' ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:464:1: (otherlv_0= 'alternative' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_videoSequences_3_0= ruleVideoSequence ) )* otherlv_4= '}' )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:464:1: (otherlv_0= 'alternative' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_videoSequences_3_0= ruleVideoSequence ) )* otherlv_4= '}' )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:464:3: otherlv_0= 'alternative' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_videoSequences_3_0= ruleVideoSequence ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleAlternatives1048); 

                	newLeafNode(otherlv_0, grammarAccess.getAlternativesAccess().getAlternativeKeyword_0());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:468:1: ( (lv_id_1_0= RULE_ID ) )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:469:1: (lv_id_1_0= RULE_ID )
            {
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:469:1: (lv_id_1_0= RULE_ID )
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:470:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAlternatives1065); 

            			newLeafNode(lv_id_1_0, grammarAccess.getAlternativesAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAlternativesRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleAlternatives1082); 

                	newLeafNode(otherlv_2, grammarAccess.getAlternativesAccess().getLeftCurlyBracketKeyword_2());
                
            // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:490:1: ( (lv_videoSequences_3_0= ruleVideoSequence ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:491:1: (lv_videoSequences_3_0= ruleVideoSequence )
            	    {
            	    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:491:1: (lv_videoSequences_3_0= ruleVideoSequence )
            	    // ../fr.istic.gla.idm.videoGen/src-gen/fr/istic/gla/idm/parser/antlr/internal/InternalVideoGen.g:492:3: lv_videoSequences_3_0= ruleVideoSequence
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAlternativesAccess().getVideoSequencesVideoSequenceParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVideoSequence_in_ruleAlternatives1103);
            	    lv_videoSequences_3_0=ruleVideoSequence();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAlternativesRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"videoSequences",
            	            		lv_videoSequences_3_0, 
            	            		"VideoSequence");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleAlternatives1116); 

                	newLeafNode(otherlv_4, grammarAccess.getAlternativesAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlternatives"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleVideoGen_in_entryRuleVideoGen75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoGen85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleVideoGen131 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleVideoGen143 = new BitSet(new long[]{0x000000000020E000L});
    public static final BitSet FOLLOW_ruleSequence_in_ruleVideoGen164 = new BitSet(new long[]{0x000000000020E000L});
    public static final BitSet FOLLOW_13_in_ruleVideoGen177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSequence_in_entryRuleSequence213 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSequence223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSingle_in_ruleSequence270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiple_in_ruleSequence297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSingle_in_entryRuleSingle332 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSingle342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_ruleSingle389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_ruleSingle416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMultiple_in_entryRuleMultiple451 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMultiple461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternatives_in_ruleMultiple507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_entryRuleMandatory541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMandatory551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleMandatory588 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_ruleMandatory609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_entryRuleOptional645 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOptional655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleOptional692 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_ruleOptional713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_entryRuleVideoSequence749 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoSequence759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleVideoSequence796 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVideoSequence813 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_17_in_ruleVideoSequence831 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVideoSequence848 = new BitSet(new long[]{0x00000000001C0000L});
    public static final BitSet FOLLOW_18_in_ruleVideoSequence868 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVideoSequence885 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_19_in_ruleVideoSequence905 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVideoSequence922 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleVideoSequence942 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVideoSequence959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternatives_in_entryRuleAlternatives1001 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAlternatives1011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleAlternatives1048 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAlternatives1065 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleAlternatives1082 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_ruleVideoSequence_in_ruleAlternatives1103 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_13_in_ruleAlternatives1116 = new BitSet(new long[]{0x0000000000000002L});

}