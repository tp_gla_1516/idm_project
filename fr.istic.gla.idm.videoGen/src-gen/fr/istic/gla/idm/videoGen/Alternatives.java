/**
 */
package fr.istic.gla.idm.videoGen;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alternatives</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.istic.gla.idm.videoGen.Alternatives#getId <em>Id</em>}</li>
 *   <li>{@link fr.istic.gla.idm.videoGen.Alternatives#getVideoSequences <em>Video Sequences</em>}</li>
 * </ul>
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getAlternatives()
 * @model
 * @generated
 */
public interface Alternatives extends Multiple
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getAlternatives_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.Alternatives#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Video Sequences</b></em>' containment reference list.
   * The list contents are of type {@link fr.istic.gla.idm.videoGen.VideoSequence}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Video Sequences</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Video Sequences</em>' containment reference list.
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getAlternatives_VideoSequences()
   * @model containment="true"
   * @generated
   */
  EList<VideoSequence> getVideoSequences();

} // Alternatives
