/**
 */
package fr.istic.gla.idm.videoGen;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiple</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getMultiple()
 * @model
 * @generated
 */
public interface Multiple extends Sequence
{
} // Multiple
