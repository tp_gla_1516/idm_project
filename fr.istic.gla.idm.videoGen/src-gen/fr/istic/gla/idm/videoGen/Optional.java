/**
 */
package fr.istic.gla.idm.videoGen;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Optional</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getOptional()
 * @model
 * @generated
 */
public interface Optional extends Single
{
} // Optional
