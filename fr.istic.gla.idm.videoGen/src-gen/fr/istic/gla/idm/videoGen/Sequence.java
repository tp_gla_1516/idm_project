/**
 */
package fr.istic.gla.idm.videoGen;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getSequence()
 * @model
 * @generated
 */
public interface Sequence extends EObject
{
} // Sequence
