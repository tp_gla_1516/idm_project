/**
 */
package fr.istic.gla.idm.videoGen;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.istic.gla.idm.videoGen.Single#getVideoSequences <em>Video Sequences</em>}</li>
 * </ul>
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getSingle()
 * @model
 * @generated
 */
public interface Single extends Sequence
{
  /**
   * Returns the value of the '<em><b>Video Sequences</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Video Sequences</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Video Sequences</em>' containment reference.
   * @see #setVideoSequences(VideoSequence)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getSingle_VideoSequences()
   * @model containment="true"
   * @generated
   */
  VideoSequence getVideoSequences();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.Single#getVideoSequences <em>Video Sequences</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Video Sequences</em>' containment reference.
   * @see #getVideoSequences()
   * @generated
   */
  void setVideoSequences(VideoSequence value);

} // Single
