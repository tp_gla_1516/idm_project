/**
 */
package fr.istic.gla.idm.videoGen;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Video Gen</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.istic.gla.idm.videoGen.VideoGen#getList <em>List</em>}</li>
 * </ul>
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoGen()
 * @model
 * @generated
 */
public interface VideoGen extends EObject
{
  /**
   * Returns the value of the '<em><b>List</b></em>' containment reference list.
   * The list contents are of type {@link fr.istic.gla.idm.videoGen.Sequence}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' containment reference list.
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoGen_List()
   * @model containment="true"
   * @generated
   */
  EList<Sequence> getList();

} // VideoGen
