/**
 */
package fr.istic.gla.idm.videoGen;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.istic.gla.idm.videoGen.VideoGenFactory
 * @model kind="package"
 * @generated
 */
public interface VideoGenPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "videoGen";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.istic.fr/gla/idm/videogen/VideoGen";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "videoGen";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  VideoGenPackage eINSTANCE = fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl.init();

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.VideoGenImpl <em>Video Gen</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getVideoGen()
   * @generated
   */
  int VIDEO_GEN = 0;

  /**
   * The feature id for the '<em><b>List</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_GEN__LIST = 0;

  /**
   * The number of structural features of the '<em>Video Gen</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_GEN_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.SequenceImpl <em>Sequence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.SequenceImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getSequence()
   * @generated
   */
  int SEQUENCE = 1;

  /**
   * The number of structural features of the '<em>Sequence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.SingleImpl <em>Single</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.SingleImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getSingle()
   * @generated
   */
  int SINGLE = 2;

  /**
   * The feature id for the '<em><b>Video Sequences</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE__VIDEO_SEQUENCES = SEQUENCE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Single</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SINGLE_FEATURE_COUNT = SEQUENCE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.MultipleImpl <em>Multiple</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.MultipleImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getMultiple()
   * @generated
   */
  int MULTIPLE = 3;

  /**
   * The number of structural features of the '<em>Multiple</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MULTIPLE_FEATURE_COUNT = SEQUENCE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.MandatoryImpl <em>Mandatory</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.MandatoryImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getMandatory()
   * @generated
   */
  int MANDATORY = 4;

  /**
   * The feature id for the '<em><b>Video Sequences</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MANDATORY__VIDEO_SEQUENCES = SINGLE__VIDEO_SEQUENCES;

  /**
   * The number of structural features of the '<em>Mandatory</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MANDATORY_FEATURE_COUNT = SINGLE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.OptionalImpl <em>Optional</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.OptionalImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getOptional()
   * @generated
   */
  int OPTIONAL = 5;

  /**
   * The feature id for the '<em><b>Video Sequences</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPTIONAL__VIDEO_SEQUENCES = SINGLE__VIDEO_SEQUENCES;

  /**
   * The number of structural features of the '<em>Optional</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPTIONAL_FEATURE_COUNT = SINGLE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.VideoSequenceImpl <em>Video Sequence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.VideoSequenceImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getVideoSequence()
   * @generated
   */
  int VIDEO_SEQUENCE = 6;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQUENCE__ID = 0;

  /**
   * The feature id for the '<em><b>Duration</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQUENCE__DURATION = 1;

  /**
   * The feature id for the '<em><b>Nom Video</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQUENCE__NOM_VIDEO = 2;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQUENCE__DESCRIPTION = 3;

  /**
   * The feature id for the '<em><b>Url</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQUENCE__URL = 4;

  /**
   * The number of structural features of the '<em>Video Sequence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQUENCE_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link fr.istic.gla.idm.videoGen.impl.AlternativesImpl <em>Alternatives</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.istic.gla.idm.videoGen.impl.AlternativesImpl
   * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getAlternatives()
   * @generated
   */
  int ALTERNATIVES = 7;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTERNATIVES__ID = MULTIPLE_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Video Sequences</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTERNATIVES__VIDEO_SEQUENCES = MULTIPLE_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Alternatives</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTERNATIVES_FEATURE_COUNT = MULTIPLE_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.VideoGen <em>Video Gen</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Video Gen</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoGen
   * @generated
   */
  EClass getVideoGen();

  /**
   * Returns the meta object for the containment reference list '{@link fr.istic.gla.idm.videoGen.VideoGen#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>List</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoGen#getList()
   * @see #getVideoGen()
   * @generated
   */
  EReference getVideoGen_List();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.Sequence <em>Sequence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sequence</em>'.
   * @see fr.istic.gla.idm.videoGen.Sequence
   * @generated
   */
  EClass getSequence();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.Single <em>Single</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Single</em>'.
   * @see fr.istic.gla.idm.videoGen.Single
   * @generated
   */
  EClass getSingle();

  /**
   * Returns the meta object for the containment reference '{@link fr.istic.gla.idm.videoGen.Single#getVideoSequences <em>Video Sequences</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Video Sequences</em>'.
   * @see fr.istic.gla.idm.videoGen.Single#getVideoSequences()
   * @see #getSingle()
   * @generated
   */
  EReference getSingle_VideoSequences();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.Multiple <em>Multiple</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Multiple</em>'.
   * @see fr.istic.gla.idm.videoGen.Multiple
   * @generated
   */
  EClass getMultiple();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.Mandatory <em>Mandatory</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mandatory</em>'.
   * @see fr.istic.gla.idm.videoGen.Mandatory
   * @generated
   */
  EClass getMandatory();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.Optional <em>Optional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Optional</em>'.
   * @see fr.istic.gla.idm.videoGen.Optional
   * @generated
   */
  EClass getOptional();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.VideoSequence <em>Video Sequence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Video Sequence</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoSequence
   * @generated
   */
  EClass getVideoSequence();

  /**
   * Returns the meta object for the attribute '{@link fr.istic.gla.idm.videoGen.VideoSequence#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoSequence#getId()
   * @see #getVideoSequence()
   * @generated
   */
  EAttribute getVideoSequence_Id();

  /**
   * Returns the meta object for the attribute '{@link fr.istic.gla.idm.videoGen.VideoSequence#getDuration <em>Duration</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Duration</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoSequence#getDuration()
   * @see #getVideoSequence()
   * @generated
   */
  EAttribute getVideoSequence_Duration();

  /**
   * Returns the meta object for the attribute '{@link fr.istic.gla.idm.videoGen.VideoSequence#getNomVideo <em>Nom Video</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nom Video</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoSequence#getNomVideo()
   * @see #getVideoSequence()
   * @generated
   */
  EAttribute getVideoSequence_NomVideo();

  /**
   * Returns the meta object for the attribute '{@link fr.istic.gla.idm.videoGen.VideoSequence#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoSequence#getDescription()
   * @see #getVideoSequence()
   * @generated
   */
  EAttribute getVideoSequence_Description();

  /**
   * Returns the meta object for the attribute '{@link fr.istic.gla.idm.videoGen.VideoSequence#getUrl <em>Url</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Url</em>'.
   * @see fr.istic.gla.idm.videoGen.VideoSequence#getUrl()
   * @see #getVideoSequence()
   * @generated
   */
  EAttribute getVideoSequence_Url();

  /**
   * Returns the meta object for class '{@link fr.istic.gla.idm.videoGen.Alternatives <em>Alternatives</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alternatives</em>'.
   * @see fr.istic.gla.idm.videoGen.Alternatives
   * @generated
   */
  EClass getAlternatives();

  /**
   * Returns the meta object for the attribute '{@link fr.istic.gla.idm.videoGen.Alternatives#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see fr.istic.gla.idm.videoGen.Alternatives#getId()
   * @see #getAlternatives()
   * @generated
   */
  EAttribute getAlternatives_Id();

  /**
   * Returns the meta object for the containment reference list '{@link fr.istic.gla.idm.videoGen.Alternatives#getVideoSequences <em>Video Sequences</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Video Sequences</em>'.
   * @see fr.istic.gla.idm.videoGen.Alternatives#getVideoSequences()
   * @see #getAlternatives()
   * @generated
   */
  EReference getAlternatives_VideoSequences();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  VideoGenFactory getVideoGenFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.VideoGenImpl <em>Video Gen</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getVideoGen()
     * @generated
     */
    EClass VIDEO_GEN = eINSTANCE.getVideoGen();

    /**
     * The meta object literal for the '<em><b>List</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO_GEN__LIST = eINSTANCE.getVideoGen_List();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.SequenceImpl <em>Sequence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.SequenceImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getSequence()
     * @generated
     */
    EClass SEQUENCE = eINSTANCE.getSequence();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.SingleImpl <em>Single</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.SingleImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getSingle()
     * @generated
     */
    EClass SINGLE = eINSTANCE.getSingle();

    /**
     * The meta object literal for the '<em><b>Video Sequences</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SINGLE__VIDEO_SEQUENCES = eINSTANCE.getSingle_VideoSequences();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.MultipleImpl <em>Multiple</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.MultipleImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getMultiple()
     * @generated
     */
    EClass MULTIPLE = eINSTANCE.getMultiple();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.MandatoryImpl <em>Mandatory</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.MandatoryImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getMandatory()
     * @generated
     */
    EClass MANDATORY = eINSTANCE.getMandatory();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.OptionalImpl <em>Optional</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.OptionalImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getOptional()
     * @generated
     */
    EClass OPTIONAL = eINSTANCE.getOptional();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.VideoSequenceImpl <em>Video Sequence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.VideoSequenceImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getVideoSequence()
     * @generated
     */
    EClass VIDEO_SEQUENCE = eINSTANCE.getVideoSequence();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQUENCE__ID = eINSTANCE.getVideoSequence_Id();

    /**
     * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQUENCE__DURATION = eINSTANCE.getVideoSequence_Duration();

    /**
     * The meta object literal for the '<em><b>Nom Video</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQUENCE__NOM_VIDEO = eINSTANCE.getVideoSequence_NomVideo();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQUENCE__DESCRIPTION = eINSTANCE.getVideoSequence_Description();

    /**
     * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQUENCE__URL = eINSTANCE.getVideoSequence_Url();

    /**
     * The meta object literal for the '{@link fr.istic.gla.idm.videoGen.impl.AlternativesImpl <em>Alternatives</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.istic.gla.idm.videoGen.impl.AlternativesImpl
     * @see fr.istic.gla.idm.videoGen.impl.VideoGenPackageImpl#getAlternatives()
     * @generated
     */
    EClass ALTERNATIVES = eINSTANCE.getAlternatives();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ALTERNATIVES__ID = eINSTANCE.getAlternatives_Id();

    /**
     * The meta object literal for the '<em><b>Video Sequences</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALTERNATIVES__VIDEO_SEQUENCES = eINSTANCE.getAlternatives_VideoSequences();

  }

} //VideoGenPackage
