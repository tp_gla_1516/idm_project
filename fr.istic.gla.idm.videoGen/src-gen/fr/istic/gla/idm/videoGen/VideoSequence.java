/**
 */
package fr.istic.gla.idm.videoGen;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Video Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.istic.gla.idm.videoGen.VideoSequence#getId <em>Id</em>}</li>
 *   <li>{@link fr.istic.gla.idm.videoGen.VideoSequence#getDuration <em>Duration</em>}</li>
 *   <li>{@link fr.istic.gla.idm.videoGen.VideoSequence#getNomVideo <em>Nom Video</em>}</li>
 *   <li>{@link fr.istic.gla.idm.videoGen.VideoSequence#getDescription <em>Description</em>}</li>
 *   <li>{@link fr.istic.gla.idm.videoGen.VideoSequence#getUrl <em>Url</em>}</li>
 * </ul>
 *
 * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoSequence()
 * @model
 * @generated
 */
public interface VideoSequence extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoSequence_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.VideoSequence#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Duration</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Duration</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Duration</em>' attribute.
   * @see #setDuration(String)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoSequence_Duration()
   * @model
   * @generated
   */
  String getDuration();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.VideoSequence#getDuration <em>Duration</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Duration</em>' attribute.
   * @see #getDuration()
   * @generated
   */
  void setDuration(String value);

  /**
   * Returns the value of the '<em><b>Nom Video</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nom Video</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nom Video</em>' attribute.
   * @see #setNomVideo(String)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoSequence_NomVideo()
   * @model
   * @generated
   */
  String getNomVideo();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.VideoSequence#getNomVideo <em>Nom Video</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nom Video</em>' attribute.
   * @see #getNomVideo()
   * @generated
   */
  void setNomVideo(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoSequence_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.VideoSequence#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Url</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Url</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Url</em>' attribute.
   * @see #setUrl(String)
   * @see fr.istic.gla.idm.videoGen.VideoGenPackage#getVideoSequence_Url()
   * @model
   * @generated
   */
  String getUrl();

  /**
   * Sets the value of the '{@link fr.istic.gla.idm.videoGen.VideoSequence#getUrl <em>Url</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Url</em>' attribute.
   * @see #getUrl()
   * @generated
   */
  void setUrl(String value);

} // VideoSequence
