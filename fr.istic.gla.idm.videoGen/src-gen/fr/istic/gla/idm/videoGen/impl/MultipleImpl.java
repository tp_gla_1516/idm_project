/**
 */
package fr.istic.gla.idm.videoGen.impl;

import fr.istic.gla.idm.videoGen.Multiple;
import fr.istic.gla.idm.videoGen.VideoGenPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multiple</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MultipleImpl extends SequenceImpl implements Multiple
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MultipleImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return VideoGenPackage.Literals.MULTIPLE;
  }

} //MultipleImpl
