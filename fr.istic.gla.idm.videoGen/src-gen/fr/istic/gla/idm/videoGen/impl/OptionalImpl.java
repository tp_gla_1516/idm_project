/**
 */
package fr.istic.gla.idm.videoGen.impl;

import fr.istic.gla.idm.videoGen.Optional;
import fr.istic.gla.idm.videoGen.VideoGenPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Optional</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OptionalImpl extends SingleImpl implements Optional
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OptionalImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return VideoGenPackage.Literals.OPTIONAL;
  }

} //OptionalImpl
