/**
 */
package fr.istic.gla.idm.videoGen.impl;

import fr.istic.gla.idm.videoGen.Single;
import fr.istic.gla.idm.videoGen.VideoGenPackage;
import fr.istic.gla.idm.videoGen.VideoSequence;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.istic.gla.idm.videoGen.impl.SingleImpl#getVideoSequences <em>Video Sequences</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleImpl extends SequenceImpl implements Single
{
  /**
   * The cached value of the '{@link #getVideoSequences() <em>Video Sequences</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVideoSequences()
   * @generated
   * @ordered
   */
  protected VideoSequence videoSequences;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SingleImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return VideoGenPackage.Literals.SINGLE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VideoSequence getVideoSequences()
  {
    return videoSequences;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVideoSequences(VideoSequence newVideoSequences, NotificationChain msgs)
  {
    VideoSequence oldVideoSequences = videoSequences;
    videoSequences = newVideoSequences;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VideoGenPackage.SINGLE__VIDEO_SEQUENCES, oldVideoSequences, newVideoSequences);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVideoSequences(VideoSequence newVideoSequences)
  {
    if (newVideoSequences != videoSequences)
    {
      NotificationChain msgs = null;
      if (videoSequences != null)
        msgs = ((InternalEObject)videoSequences).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VideoGenPackage.SINGLE__VIDEO_SEQUENCES, null, msgs);
      if (newVideoSequences != null)
        msgs = ((InternalEObject)newVideoSequences).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VideoGenPackage.SINGLE__VIDEO_SEQUENCES, null, msgs);
      msgs = basicSetVideoSequences(newVideoSequences, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, VideoGenPackage.SINGLE__VIDEO_SEQUENCES, newVideoSequences, newVideoSequences));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case VideoGenPackage.SINGLE__VIDEO_SEQUENCES:
        return basicSetVideoSequences(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case VideoGenPackage.SINGLE__VIDEO_SEQUENCES:
        return getVideoSequences();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case VideoGenPackage.SINGLE__VIDEO_SEQUENCES:
        setVideoSequences((VideoSequence)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case VideoGenPackage.SINGLE__VIDEO_SEQUENCES:
        setVideoSequences((VideoSequence)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case VideoGenPackage.SINGLE__VIDEO_SEQUENCES:
        return videoSequences != null;
    }
    return super.eIsSet(featureID);
  }

} //SingleImpl
